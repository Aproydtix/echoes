﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelControl : MonoBehaviour {

	public GameObject[] levelNr;

	// Use this for initialization
	void Start () {
		for (int i=0; i < levelNr.Length; i++){
			levelNr [i].GetComponent<Text> ().enabled = false;
		}
		levelNr [SceneManager.GetActiveScene ().buildIndex-1].GetComponent<Text> ().enabled = true;
		//levelNr [0].GetComponent<Text> ().enabled = true;
	}
	
	// Update is called once per frame
	public void DeathText () {
		/*if (Input.GetKeyDown(KeyCode.E))*/{
			StartCoroutine (PlayerDied());
		}
	}

	IEnumerator PlayerDied (){
		levelNr [3].GetComponent<Animator> ().SetInteger ("animationstate", 1);
		levelNr [3].GetComponent<Text> ().enabled = true;
		yield return new WaitForSeconds (3);
		float fadeTime = GameObject.Find("Goal").GetComponent<Fading> ().BeginFade (1);
		yield return new WaitForSeconds (fadeTime);
		SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex);
	}

}
