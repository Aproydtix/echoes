﻿using System.Collections;
using System.Collections.Generic;
using GamepadInput;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour {

    bool moving;
    public float maxspeed;
    public float minspeed;
    public float camRotSpeed;
    public float cooldownDuration = 1f;
    float cooldown;
    public float gamepadDeadzone = 0.1f;
    Rigidbody rb;
    public GameObject cam;
	public GameObject echoShot;
    Vector2 prevMousePos;
    public AudioClip footstep;
    bool dead;
    public GameObject deadText;

    // Use this for initialization
    void Start ()
    {
        rb = GetComponent<Rigidbody>();
        prevMousePos = Input.mousePosition;
        GetComponent<AudioSource>().clip = footstep;
        deadText = GameObject.Find("Main_Canvas");
    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        if (dead)
        {
            deadText.GetComponent<LevelControl>().DeathText();
            rb.velocity = Vector3.zero;
            return;
        }
        //Camera
        cam.transform.localEulerAngles = new Vector3(cam.transform.localEulerAngles.x, 0, 0);   //Locks camera rotation
        Vector2 cameraMovement = GamePad.GetAxis(GamePad.Axis.RightStick, GamePad.Index.Any);   //Gamepad Input
        Vector2 cameraMovementMouse = ((Vector2)Input.mousePosition - prevMousePos)/10f;            //Mouse Input
        if (cameraMovement.magnitude > gamepadDeadzone)                                         //Deadzone
        {
            Camera(cameraMovement);
        }
        else if (cameraMovementMouse.magnitude > gamepadDeadzone)
        {
            Camera(cameraMovementMouse);
        }

        prevMousePos = Input.mousePosition;

        //Keyboard Input
        int left =  (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))?1:0;
        int right = (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))?1:0;
        int up =    (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))?1:0;
        int down =  (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))?1:0;

        //Movement
        Vector2 direction = GamePad.GetAxis(GamePad.Axis.LeftStick, GamePad.Index.Any);
        Vector2 directionKB = new Vector2(right-left, up-down);
        if (direction.magnitude > gamepadDeadzone)                      //Check gamepad deadzone
        {
            Move(direction);
        }
        else if (directionKB.magnitude > gamepadDeadzone)
        {
            Move(directionKB);
        }
        else
        {
            rb.velocity *= .8f;                                         //Deccelerate
            if (rb.velocity.magnitude < minspeed)
                rb.velocity = Vector3.zero;                             //Stop complete below minspeed
        }

        if (cooldown > 0)
            cooldown -= Time.fixedDeltaTime;

        //Echo
        if (GamePad.GetButton(GamePad.Button.RightShoulder, GamePad.Index.Any) ||
            GamePad.GetButton(GamePad.Button.LeftShoulder, GamePad.Index.Any) ||
            Input.GetKey(KeyCode.Space) ||
            Input.GetMouseButton(0))
        {
            if (cooldown <= 0)
            {
                cooldown = cooldownDuration;
                Instantiate(echoShot, new Vector3(cam.transform.position.x, cam.transform.position.y-.1f, cam.transform.position.z), transform.rotation);
            }
		}
        
    }

    void Camera(Vector2 input)
    {
        transform.localEulerAngles += new Vector3(0, input.x * camRotSpeed, 0);    //
        cam.transform.localEulerAngles += new Vector3(-input.y * camRotSpeed, 0, 0);
        if (cam.transform.localEulerAngles.x < 180)
            cam.transform.localEulerAngles = new Vector3(Mathf.Clamp(cam.transform.localEulerAngles.x, -60, 60), 0, 0);
    }

    void Move(Vector2 input)
    {
        rb.velocity += new Vector3(                                 //Accelerated movement
                input.x * Mathf.Cos(transform.localEulerAngles.y * Mathf.Deg2Rad) + input.y * Mathf.Sin(transform.localEulerAngles.y * Mathf.Deg2Rad),
                0,
                input.y * Mathf.Cos(transform.localEulerAngles.y * Mathf.Deg2Rad) - input.x * Mathf.Sin(transform.localEulerAngles.y * Mathf.Deg2Rad)
                );
        if (rb.velocity.magnitude > maxspeed * input.magnitude)
            rb.velocity = rb.velocity.normalized * maxspeed * input.magnitude;        //Limit speed to maxspeed

        GetComponent<AudioSource>().volume = rb.velocity.magnitude / 30f;
        GetComponent<AudioSource>().pitch = (.9f + GetComponent<AudioSource>().volume / 2f) * Random.Range(.9f, 1.1f);
        if (!GetComponent<AudioSource>().isPlaying)
            GetComponent<AudioSource>().Play();
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Enemy")
        {
            dead = true;
        }
    }
}
