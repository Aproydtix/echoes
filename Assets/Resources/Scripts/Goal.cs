﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Goal : MonoBehaviour {

    public GameObject winText;

    IEnumerator OnTriggerEnter (Collider other){
		if (other.gameObject.tag == "Player"){
            if (SceneManager.GetActiveScene().buildIndex == 3)
            { 
                winText.GetComponent<Animator>().SetInteger("animationstate", 1);
                winText.GetComponent<Text>().enabled = true;
                yield return new WaitForSeconds(3);
            }
            float fadeTime = gameObject.GetComponent<Fading> ().BeginFade (1);
			yield return new WaitForSeconds (fadeTime);
            if (SceneManager.GetActiveScene().buildIndex != 3)
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            
        }
	}

}
