﻿using UnityEngine;
using System.Collections;

public class Echo : MonoBehaviour {

	// Max duration of echo after triggering
	public float duration;
	// Light that emits from echo
	public GameObject echoParticle;
	//public GameObject echoBack;
	// Check if something is hit by echo
	bool hasTriggered = false;

	void Awake (){
		Instantiate (echoParticle, transform.position, transform.rotation);
	}

	// Use this for initialization
	void Start () {
		//Instantiate (echoParticle, transform.position, transform.rotation);

		// Echo speed
		gameObject.GetComponent<Rigidbody> ().AddForce (transform.forward * 400);
		StartCoroutine (EchoWave());
	}
		
	IEnumerator EchoWave (){
		yield return new WaitForSeconds (0.3f);
		gameObject.GetComponent<EchoLight> ().turnUp = false;
		gameObject.GetComponent<EchoLight> ().isActive = true;
		yield return new WaitForSeconds (duration);
		Destroy (this.gameObject);
	}

	// Spawn light if triggered
	void OnTriggerEnter(Collider other){
		if (other.gameObject.tag != "floor" && other.gameObject.tag != "Player" && other.gameObject.tag != "echo"){
			//this.gameObject.GetComponent<Rigidbody> ().AddForce (transform.forward * -390);
			gameObject.GetComponent<Rigidbody> ().velocity = new Vector3 (0,0,0);
//			if (gameObject.GetComponent<EchoLight> ().lightIntensity < 1.5f){
//				gameObject.GetComponent<EchoLight> ().lightIntensity = 1.5f;
//			}

			//gameObject.GetComponentInChildren<EchoLight> ().lightIntensity = 2f;
			//hasTriggered = true;
			//StartCoroutine (EchoWave());
		}
	}

}
