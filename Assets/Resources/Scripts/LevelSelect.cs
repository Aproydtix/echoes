﻿using System.Collections;
using System.Collections.Generic;
using GamepadInput;
using UnityEngine;

public class LevelSelect : MonoBehaviour {

    public int selected;
    float delay;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (delay <= 0)
        {
            if (Mathf.Abs(GamePad.GetAxis(GamePad.Axis.LeftStick, GamePad.Index.Any).y) > .1f)
            {
                selected -= (int)Mathf.Sign(GamePad.GetAxis(GamePad.Axis.LeftStick, GamePad.Index.Any).y);
                delay = .3f;
            }
            else if (Mathf.Abs(GamePad.GetAxis(GamePad.Axis.Dpad, GamePad.Index.Any).y) > .1f)
            {
                selected -= (int)Mathf.Sign(GamePad.GetAxis(GamePad.Axis.Dpad, GamePad.Index.Any).y);
                delay = .3f;
            }
        }
        else
            delay -= Time.deltaTime;

        if (Mathf.Abs(GamePad.GetAxis(GamePad.Axis.LeftStick, GamePad.Index.Any).y) < .1f && Mathf.Abs(GamePad.GetAxis(GamePad.Axis.Dpad, GamePad.Index.Any).y) < .1f)
            delay = 0;

        if (selected > 3) selected = 0;
        if (selected < 0) selected = 3;

        if (GamePad.GetButtonDown(GamePad.Button.A, GamePad.Index.Any) || GamePad.GetButtonDown(GamePad.Button.Start, GamePad.Index.Any))
        {
            if (selected != 3)
                Application.LoadLevel(selected + 1);
            else
            {
                Application.Quit();
#if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
#endif
            }
        }
	}

    void OnGUI()
    {
        float w = Screen.width / 1920f;
        float h = Screen.height / 1080f;

        if (selected == 0) GUI.color = Color.white; else GUI.color = Color.black;
        if (GUI.Button(new Rect(w*1920/ 2 - w * 200, h*100, w*400, h*150), "Level 1")) Application.LoadLevel(1);
        if (selected == 1) GUI.color = Color.white; else GUI.color = Color.black;
        if (GUI.Button(new Rect(w*1920/ 2 - w * 200, h*300, w*400, h*150), "Level 2")) Application.LoadLevel(2);
        if (selected == 2) GUI.color = Color.white; else GUI.color = Color.black;
        if (GUI.Button(new Rect(w*1920/ 2 - w * 200, h*500, w*400, h*150), "Level 3")) Application.LoadLevel(3);
        if (selected == 3) GUI.color = Color.white; else GUI.color = Color.black;
        if (GUI.Button(new Rect(w*1920/ 2 - w * 200, h*700, w*400, h*150), "Exit"))
                    {
                        Application.Quit();
#if UNITY_EDITOR
                        UnityEditor.EditorApplication.isPlaying = false;
#endif
                    }
    }
}
