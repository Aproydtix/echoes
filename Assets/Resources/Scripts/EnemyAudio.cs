﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAudio : MonoBehaviour {

    AudioSource as1;
    AudioSource as2;
    NavMeshAgent nav;

    // Use this for initialization
    void Start ()
    {
        as1 = GetComponents<AudioSource>()[0];
        as2 = GetComponents<AudioSource>()[1];
        nav = GetComponent<NavMeshAgent>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (!as1.isPlaying)
        {
            as1.volume = Random.Range(.5f, 75f);
            as1.pitch = Random.Range(.9f, 1.1f);
            as1.Play();
        }


        as2.volume = nav.velocity.magnitude / 2.5f;
        as2.pitch = (.9f + GetComponent<AudioSource>().volume / 2f) * Random.Range(.9f, 1.1f);
        if (!as2.isPlaying)
            as2.Play();
    }
}
