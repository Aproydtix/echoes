﻿using UnityEngine;
using System.Collections;

public class Box : MonoBehaviour {

	//bool isHit = false;

	// Use this for initialization
	void Start () {
		// deactivate by default(on level startup)
		gameObject.GetComponent<MeshRenderer> ().enabled = false;
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter(Collider other){
		// activate when triggered by echo
		if(other.gameObject.tag == "echo")
			gameObject.GetComponent<MeshRenderer> ().enabled = true;
	}

}
