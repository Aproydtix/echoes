﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialController : MonoBehaviour {

	public GameObject[] uiElements;

	public bool[] steps;

	// Use this for initialization
	void Start () {
		for (int i=0; i<1; i++){
			steps [i] = true;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Space)){
			uiElements [0].GetComponent<Text> ().enabled = false;
		}
	}
}
