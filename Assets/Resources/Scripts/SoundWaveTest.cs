﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundWaveTest : MonoBehaviour {

	MoveTo move;

	// Use this for initialization
	void Start () {
		move = GameObject.FindGameObjectWithTag ("Player").GetComponent<MoveTo> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Q)){
			move.StartMoving(this.gameObject);
		}
	}
}
