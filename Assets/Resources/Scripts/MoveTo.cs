﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MoveTo : MonoBehaviour {

	// controller for Nav-agent
	public NavMeshAgent agent;

	// duration of walking towards target
	public float walkDuration;

	// Use this for initialization
	void Start () {
		agent = GetComponent<NavMeshAgent> ();
	}

	public void StartMoving (GameObject target){
		StartCoroutine (MoveTowardsSound(target));
	}

	IEnumerator MoveTowardsSound (GameObject target){
        // setting destination
        if (Vector3.Distance(agent.destination, target.transform.position) < 15 /*|| Application.loadedLevel == 3*/)
            agent.destination = target.transform.position;

            agent.Resume();
            yield return new WaitForSeconds(walkDuration);
            agent.Stop();
        
	}

}
