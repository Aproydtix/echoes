﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightsOff : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
        GetComponent<Light>().intensity = 0;
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
}
