﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIScript : MonoBehaviour {

    public Texture2D texture = null;
    public float angle = 0;
    public Vector2 size = new Vector2(128, 128);
    Vector2 pos = Vector2.zero;
    Rect rect;
    Vector2 pivot;

    void UpdateCompass()
    {
        float w = Screen.width / 1920f;
        float h = Screen.height / 1080f;
        size = new Vector2(w * 200, h * 200);
        pos = new Vector2(w * 1920 * .9f, h * 1080 * .85f);
        rect = new Rect(pos.x - size.x * 0.5f, pos.y - size.y * 0.5f, size.x, size.y);
        pivot = new Vector2(rect.xMin + rect.width * 0.5f, rect.yMin + rect.height * 0.5f);
        //angle = -GameObject.Find("Player").transform.eulerAngles.y;
        if ((GameObject.Find("Player").transform.position.z - GameObject.Find("Goal").transform.position.z) < 0)
            angle = -GameObject.Find("Player").transform.eulerAngles.y + Vector3.Angle(GameObject.Find("Player").transform.position - GameObject.Find("Goal").transform.position, Vector3.right);
        else
            angle = -GameObject.Find("Player").transform.eulerAngles.y - Vector3.Angle(GameObject.Find("Player").transform.position - GameObject.Find("Goal").transform.position, Vector3.right);
        angle -= 90;
    }

    void OnGUI()
    {
        UpdateCompass();
        Matrix4x4 matrixBackup = GUI.matrix;
        GUIUtility.RotateAroundPivot(angle, pivot);
        GUI.DrawTexture(rect, texture);
        GUI.matrix = matrixBackup;
    }
}
