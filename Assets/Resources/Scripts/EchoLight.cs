﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EchoLight : MonoBehaviour {
    // light intensity
    public float lightIntensity = 0f;
	// check if active
	public bool isActive = false;
	public bool turnUp = true;
	// Reference to enemy MoveTo script
	GameObject[] move;

	// Use this for initialization
	void Start () {
		lightIntensity = 0f;
		move = GameObject.FindGameObjectsWithTag("Enemy");
        for (int i=0; i < move.Length; i++)
		    move[i].GetComponent<MoveTo>().StartMoving(this.gameObject);
		//LightControl();
	}

	void Update (){
		gameObject.GetComponent<Light> ().intensity = lightIntensity;

		// adjust intensity when active
		if (turnUp && lightIntensity < .75f){
			lightIntensity += 0.05f;
		}

		if (isActive){
			lightIntensity -= 0.01f;
		}

		if (isActive && lightIntensity <= 0){
			Destroy (this.gameObject);
		}
	}

	/*void LightControl(){
		// Delay before setting light

		gameObject.GetComponent<Light> ().intensity = 2.5f;
		isActive = true;

	}*/

}
