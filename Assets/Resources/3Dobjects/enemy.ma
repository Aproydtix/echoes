//Maya ASCII 2016 scene
//Name: enemy.ma
//Last modified: Fri, Jan 20, 2017 10:40:23 PM
//Codeset: 1252
requires maya "2016";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2016";
fileInfo "version" "2016";
fileInfo "cutIdentifier" "201502261600-953408";
fileInfo "osv" "Microsoft Windows 8 Home Premium Edition, 64-bit  (Build 9200)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "FA287706-4EED-3BEB-61B4-E49621E70681";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -493.35517307231225 250.08071340235642 261.88688579256143 ;
	setAttr ".r" -type "double3" 1431.8616472578692 -419.39999999947975 -2.3430470854171077e-015 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "96FFDE7C-43BA-9468-1552-49B8BAE674C9";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 582.5329312059323;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "B0D106E1-40EF-94FA-89CB-6FB22450DBFD";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1200.0999999999999 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "1361F9E8-4909-D986-753A-6DBF75E33C52";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1200.0999999999999;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "D9387422-4A37-3D3F-495F-95AC6F4331CB";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 1200.0999999999999 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "E442655E-4171-A0B1-B7A3-98ABB2FDF632";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1200.0999999999999;
	setAttr ".ow" 68.736010267225907;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "684B4218-42B8-9044-5CAF-64AB26BEECAA";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1200.0999999999999 17.573150701107334 2.629253235776714 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "2CCC26DA-4C93-7E33-D499-2AA87CE7015F";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1200.0999999999999;
	setAttr ".ow" 13.222353345739899;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "Enemy";
	rename -uid "7D986746-4D89-B625-7721-E4A4E29AE02D";
	setAttr ".t" -type "double3" -5.6984214001240152e-014 465.3114191039798 0 ;
	setAttr ".r" -type "double3" 0 0 -180 ;
	setAttr ".s" -type "double3" 14.419382791921279 14.419382791921279 14.419382791921279 ;
	setAttr ".rp" -type "double3" 5.9644340548414345e-031 462.72582021403491 0 ;
	setAttr ".rpt" -type "double3" 5.666756946079519e-014 -925.45164042806982 0 ;
	setAttr ".sp" -type "double3" 4.1364003861407486e-032 32.090542770893492 0 ;
	setAttr ".spt" -type "double3" 5.5507940162272598e-031 430.6352774431416 0 ;
createNode transform -n "pCube1" -p "Enemy";
	rename -uid "172542AD-4250-D252-F077-1F9A040B4ED8";
	setAttr ".t" -type "double3" 0 20.765924948942121 0 ;
	setAttr ".s" -type "double3" 6.2568508953739101 6.2568508953739101 6.2568508953739101 ;
createNode mesh -n "pCubeShape1" -p "pCube1";
	rename -uid "574DE72B-429E-44CF-104D-0C935583B882";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.53125 0.375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
createNode transform -n "pSphere1" -p "Enemy";
	rename -uid "1352EF13-4B28-DA99-B16C-C2B99BF74F75";
	setAttr ".t" -type "double3" 2.2465702719403322 15.82465912585004 1.1915283868210895 ;
	setAttr ".s" -type "double3" 0.32521119232928131 0.32521119232928131 0.32521119232928131 ;
createNode mesh -n "pSphereShape1" -p "pSphere1";
	rename -uid "E4C13904-4DBD-F406-D375-E58ED4D600B0";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.50000005960464478 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pSphere2" -p "Enemy";
	rename -uid "95C54BF4-4E08-0277-9B7B-6384B21DC512";
	setAttr ".t" -type "double3" 2.24657642540702 15.824581927813409 -1.1914032663317666 ;
	setAttr ".s" -type "double3" 0.32521119232928131 0.32521119232928131 0.32521119232928131 ;
createNode mesh -n "pSphereShape2" -p "pSphere2";
	rename -uid "A3261DDB-4ECF-DC03-B2B7-F784A00E11FB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.50000005960464478 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode mesh -n "polySurfaceShape1" -p "pSphere2";
	rename -uid "82B2FC10-4B29-0FAC-D777-68BBA9704AA7";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 119 ".uvst[0].uvsp[0:118]" -type "float2" 0 0.1 0.1 0.1 0.2
		 0.1 0.30000001 0.1 0.40000001 0.1 0.5 0.1 0.60000002 0.1 0.70000005 0.1 0.80000007
		 0.1 0.9000001 0.1 1.000000119209 0.1 0 0.2 0.1 0.2 0.2 0.2 0.30000001 0.2 0.40000001
		 0.2 0.5 0.2 0.60000002 0.2 0.70000005 0.2 0.80000007 0.2 0.9000001 0.2 1.000000119209
		 0.2 0 0.30000001 0.1 0.30000001 0.2 0.30000001 0.30000001 0.30000001 0.40000001 0.30000001
		 0.5 0.30000001 0.60000002 0.30000001 0.70000005 0.30000001 0.80000007 0.30000001
		 0.9000001 0.30000001 1.000000119209 0.30000001 0 0.40000001 0.1 0.40000001 0.2 0.40000001
		 0.30000001 0.40000001 0.40000001 0.40000001 0.5 0.40000001 0.60000002 0.40000001
		 0.70000005 0.40000001 0.80000007 0.40000001 0.9000001 0.40000001 1.000000119209 0.40000001
		 0 0.5 0.1 0.5 0.2 0.5 0.30000001 0.5 0.40000001 0.5 0.5 0.5 0.60000002 0.5 0.70000005
		 0.5 0.80000007 0.5 0.9000001 0.5 1.000000119209 0.5 0 0.60000002 0.1 0.60000002 0.2
		 0.60000002 0.30000001 0.60000002 0.40000001 0.60000002 0.5 0.60000002 0.60000002
		 0.60000002 0.70000005 0.60000002 0.80000007 0.60000002 0.9000001 0.60000002 1.000000119209
		 0.60000002 0 0.70000005 0.1 0.70000005 0.2 0.70000005 0.30000001 0.70000005 0.40000001
		 0.70000005 0.5 0.70000005 0.60000002 0.70000005 0.70000005 0.70000005 0.80000007
		 0.70000005 0.9000001 0.70000005 1.000000119209 0.70000005 0 0.80000007 0.1 0.80000007
		 0.2 0.80000007 0.30000001 0.80000007 0.40000001 0.80000007 0.5 0.80000007 0.60000002
		 0.80000007 0.70000005 0.80000007 0.80000007 0.80000007 0.9000001 0.80000007 1.000000119209
		 0.80000007 0 0.9000001 0.1 0.9000001 0.2 0.9000001 0.30000001 0.9000001 0.40000001
		 0.9000001 0.5 0.9000001 0.60000002 0.9000001 0.70000005 0.9000001 0.80000007 0.9000001
		 0.9000001 0.9000001 1.000000119209 0.9000001 0.050000001 0 0.15000001 0 0.25 0 0.34999999
		 0 0.45000002 0 0.55000001 0 0.65000004 0 0.75 0 0.85000002 0 0.94999999 0 0.050000001
		 1 0.15000001 1 0.25 1 0.34999999 1 0.45000002 1 0.55000001 1 0.65000004 1 0.75 1
		 0.85000002 1 0.94999999 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 92 ".vt[0:91]"  0.25000003 -0.95105654 -0.18163569 0.095491491 -0.95105654 -0.29389271
		 -0.095491551 -0.95105654 -0.29389265 -0.25000006 -0.95105654 -0.18163563 -0.30901703 -0.95105654 1.8418849e-008
		 -0.25000003 -0.95105654 0.18163568 -0.095491499 -0.95105654 0.29389265 0.095491514 -0.95105654 0.29389265
		 0.25 -0.95105654 0.18163563 0.309017 -0.95105654 0 0.4755283 -0.809017 -0.34549159
		 0.1816356 -0.809017 -0.55901712 -0.18163572 -0.809017 -0.55901706 -0.47552836 -0.809017 -0.3454915
		 -0.5877853 -0.809017 3.5034731e-008 -0.4755283 -0.809017 0.34549156 -0.18163562 -0.809017 0.55901706
		 0.18163565 -0.809017 0.559017 0.47552827 -0.809017 0.3454915 0.58778524 -0.809017 0
		 0.65450853 -0.58778524 -0.47552839 0.24999996 -0.58778524 -0.76942104 -0.25000012 -0.58778524 -0.76942098
		 -0.65450865 -0.58778524 -0.47552827 -0.80901712 -0.58778524 4.8221171e-008 -0.65450853 -0.58778524 0.47552836
		 -0.24999999 -0.58778524 0.76942098 0.25000003 -0.58778524 0.76942092 0.65450853 -0.58778524 0.47552827
		 0.809017 -0.58778524 0 0.76942098 -0.30901697 -0.55901718 0.29389259 -0.30901697 -0.90450871
		 -0.29389277 -0.30901697 -0.90450859 -0.7694211 -0.30901697 -0.559017 -0.95105666 -0.30901697 5.6687387e-008
		 -0.76942098 -0.30901697 0.55901712 -0.29389262 -0.30901697 0.90450859 0.29389268 -0.30901697 0.90450853
		 0.76942092 -0.30901697 0.559017 0.95105654 -0.30901697 0 0.80901706 0 -0.58778542
		 0.30901694 0 -0.95105672 -0.30901715 0 -0.9510566 -0.80901718 0 -0.58778524 -1.000000119209 0 5.9604645e-008
		 -0.80901706 0 0.58778536 -0.30901697 0 0.9510566 0.30901703 0 0.95105654 0.809017 0 0.58778524
		 1 0 0 0.76942098 0.30901697 -0.55901718 0.29389259 0.30901697 -0.90450871 -0.29389277 0.30901697 -0.90450859
		 -0.7694211 0.30901697 -0.559017 -0.95105666 0.30901697 5.6687387e-008 -0.76942098 0.30901697 0.55901712
		 -0.29389262 0.30901697 0.90450859 0.29389268 0.30901697 0.90450853 0.76942092 0.30901697 0.559017
		 0.95105654 0.30901697 0 0.65450853 0.58778524 -0.47552839 0.24999996 0.58778524 -0.76942104
		 -0.25000012 0.58778524 -0.76942098 -0.65450865 0.58778524 -0.47552827 -0.80901712 0.58778524 4.8221171e-008
		 -0.65450853 0.58778524 0.47552836 -0.24999999 0.58778524 0.76942098 0.25000003 0.58778524 0.76942092
		 0.65450853 0.58778524 0.47552827 0.809017 0.58778524 0 0.4755283 0.809017 -0.34549159
		 0.1816356 0.809017 -0.55901712 -0.18163572 0.809017 -0.55901706 -0.47552836 0.809017 -0.3454915
		 -0.5877853 0.809017 3.5034731e-008 -0.4755283 0.809017 0.34549156 -0.18163562 0.809017 0.55901706
		 0.18163565 0.809017 0.559017 0.47552827 0.809017 0.3454915 0.58778524 0.809017 0
		 0.25000003 0.95105654 -0.18163569 0.095491491 0.95105654 -0.29389271 -0.095491551 0.95105654 -0.29389265
		 -0.25000006 0.95105654 -0.18163563 -0.30901703 0.95105654 1.8418849e-008 -0.25000003 0.95105654 0.18163568
		 -0.095491499 0.95105654 0.29389265 0.095491514 0.95105654 0.29389265 0.25 0.95105654 0.18163563
		 0.309017 0.95105654 0 0 -1 0 0 1 0;
	setAttr -s 190 ".ed";
	setAttr ".ed[0:165]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0 7 8 0 8 9 0
		 9 0 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 16 0 16 17 0 17 18 0 18 19 0 19 10 0
		 20 21 0 21 22 0 22 23 0 23 24 0 24 25 0 25 26 0 26 27 0 27 28 0 28 29 0 29 20 0 30 31 0
		 31 32 0 32 33 0 33 34 0 34 35 0 35 36 0 36 37 0 37 38 0 38 39 0 39 30 0 40 41 0 41 42 0
		 42 43 0 43 44 0 44 45 0 45 46 0 46 47 0 47 48 0 48 49 0 49 40 0 50 51 0 51 52 0 52 53 0
		 53 54 0 54 55 0 55 56 0 56 57 0 57 58 0 58 59 0 59 50 0 60 61 0 61 62 0 62 63 0 63 64 0
		 64 65 0 65 66 0 66 67 0 67 68 0 68 69 0 69 60 0 70 71 0 71 72 0 72 73 0 73 74 0 74 75 0
		 75 76 0 76 77 0 77 78 0 78 79 0 79 70 0 80 81 0 81 82 0 82 83 0 83 84 0 84 85 0 85 86 0
		 86 87 0 87 88 0 88 89 0 89 80 0 0 10 0 1 11 0 2 12 0 3 13 0 4 14 0 5 15 0 6 16 0
		 7 17 0 8 18 0 9 19 0 10 20 0 11 21 0 12 22 0 13 23 0 14 24 0 15 25 0 16 26 0 17 27 0
		 18 28 0 19 29 0 20 30 0 21 31 0 22 32 0 23 33 0 24 34 0 25 35 0 26 36 0 27 37 0 28 38 0
		 29 39 0 30 40 0 31 41 0 32 42 0 33 43 0 34 44 0 35 45 0 36 46 0 37 47 0 38 48 0 39 49 0
		 40 50 0 41 51 0 42 52 0 43 53 0 44 54 0 45 55 0 46 56 0 47 57 0 48 58 0 49 59 0 50 60 0
		 51 61 0 52 62 0 53 63 0 54 64 0 55 65 0 56 66 0 57 67 0 58 68 0 59 69 0 60 70 0 61 71 0
		 62 72 0 63 73 0 64 74 0 65 75 0 66 76 0 67 77 0 68 78 0 69 79 0 70 80 0 71 81 0 72 82 0
		 73 83 0 74 84 0 75 85 0;
	setAttr ".ed[166:189]" 76 86 0 77 87 0 78 88 0 79 89 0 90 0 0 90 1 0 90 2 0
		 90 3 0 90 4 0 90 5 0 90 6 0 90 7 0 90 8 0 90 9 0 80 91 0 81 91 0 82 91 0 83 91 0
		 84 91 0 85 91 0 86 91 0 87 91 0 88 91 0 89 91 0;
	setAttr -s 100 -ch 380 ".fc[0:99]" -type "polyFaces" 
		f 4 0 91 -11 -91
		mu 0 4 0 1 12 11
		f 4 1 92 -12 -92
		mu 0 4 1 2 13 12
		f 4 2 93 -13 -93
		mu 0 4 2 3 14 13
		f 4 3 94 -14 -94
		mu 0 4 3 4 15 14
		f 4 4 95 -15 -95
		mu 0 4 4 5 16 15
		f 4 5 96 -16 -96
		mu 0 4 5 6 17 16
		f 4 6 97 -17 -97
		mu 0 4 6 7 18 17
		f 4 7 98 -18 -98
		mu 0 4 7 8 19 18
		f 4 8 99 -19 -99
		mu 0 4 8 9 20 19
		f 4 9 90 -20 -100
		mu 0 4 9 10 21 20
		f 4 10 101 -21 -101
		mu 0 4 11 12 23 22
		f 4 11 102 -22 -102
		mu 0 4 12 13 24 23
		f 4 12 103 -23 -103
		mu 0 4 13 14 25 24
		f 4 13 104 -24 -104
		mu 0 4 14 15 26 25
		f 4 14 105 -25 -105
		mu 0 4 15 16 27 26
		f 4 15 106 -26 -106
		mu 0 4 16 17 28 27
		f 4 16 107 -27 -107
		mu 0 4 17 18 29 28
		f 4 17 108 -28 -108
		mu 0 4 18 19 30 29
		f 4 18 109 -29 -109
		mu 0 4 19 20 31 30
		f 4 19 100 -30 -110
		mu 0 4 20 21 32 31
		f 4 20 111 -31 -111
		mu 0 4 22 23 34 33
		f 4 21 112 -32 -112
		mu 0 4 23 24 35 34
		f 4 22 113 -33 -113
		mu 0 4 24 25 36 35
		f 4 23 114 -34 -114
		mu 0 4 25 26 37 36
		f 4 24 115 -35 -115
		mu 0 4 26 27 38 37
		f 4 25 116 -36 -116
		mu 0 4 27 28 39 38
		f 4 26 117 -37 -117
		mu 0 4 28 29 40 39
		f 4 27 118 -38 -118
		mu 0 4 29 30 41 40
		f 4 28 119 -39 -119
		mu 0 4 30 31 42 41
		f 4 29 110 -40 -120
		mu 0 4 31 32 43 42
		f 4 30 121 -41 -121
		mu 0 4 33 34 45 44
		f 4 31 122 -42 -122
		mu 0 4 34 35 46 45
		f 4 32 123 -43 -123
		mu 0 4 35 36 47 46
		f 4 33 124 -44 -124
		mu 0 4 36 37 48 47
		f 4 34 125 -45 -125
		mu 0 4 37 38 49 48
		f 4 35 126 -46 -126
		mu 0 4 38 39 50 49
		f 4 36 127 -47 -127
		mu 0 4 39 40 51 50
		f 4 37 128 -48 -128
		mu 0 4 40 41 52 51
		f 4 38 129 -49 -129
		mu 0 4 41 42 53 52
		f 4 39 120 -50 -130
		mu 0 4 42 43 54 53
		f 4 40 131 -51 -131
		mu 0 4 44 45 56 55
		f 4 41 132 -52 -132
		mu 0 4 45 46 57 56
		f 4 42 133 -53 -133
		mu 0 4 46 47 58 57
		f 4 43 134 -54 -134
		mu 0 4 47 48 59 58
		f 4 44 135 -55 -135
		mu 0 4 48 49 60 59
		f 4 45 136 -56 -136
		mu 0 4 49 50 61 60
		f 4 46 137 -57 -137
		mu 0 4 50 51 62 61
		f 4 47 138 -58 -138
		mu 0 4 51 52 63 62
		f 4 48 139 -59 -139
		mu 0 4 52 53 64 63
		f 4 49 130 -60 -140
		mu 0 4 53 54 65 64
		f 4 50 141 -61 -141
		mu 0 4 55 56 67 66
		f 4 51 142 -62 -142
		mu 0 4 56 57 68 67
		f 4 52 143 -63 -143
		mu 0 4 57 58 69 68
		f 4 53 144 -64 -144
		mu 0 4 58 59 70 69
		f 4 54 145 -65 -145
		mu 0 4 59 60 71 70
		f 4 55 146 -66 -146
		mu 0 4 60 61 72 71
		f 4 56 147 -67 -147
		mu 0 4 61 62 73 72
		f 4 57 148 -68 -148
		mu 0 4 62 63 74 73
		f 4 58 149 -69 -149
		mu 0 4 63 64 75 74
		f 4 59 140 -70 -150
		mu 0 4 64 65 76 75
		f 4 60 151 -71 -151
		mu 0 4 66 67 78 77
		f 4 61 152 -72 -152
		mu 0 4 67 68 79 78
		f 4 62 153 -73 -153
		mu 0 4 68 69 80 79
		f 4 63 154 -74 -154
		mu 0 4 69 70 81 80
		f 4 64 155 -75 -155
		mu 0 4 70 71 82 81
		f 4 65 156 -76 -156
		mu 0 4 71 72 83 82
		f 4 66 157 -77 -157
		mu 0 4 72 73 84 83
		f 4 67 158 -78 -158
		mu 0 4 73 74 85 84
		f 4 68 159 -79 -159
		mu 0 4 74 75 86 85
		f 4 69 150 -80 -160
		mu 0 4 75 76 87 86
		f 4 70 161 -81 -161
		mu 0 4 77 78 89 88
		f 4 71 162 -82 -162
		mu 0 4 78 79 90 89
		f 4 72 163 -83 -163
		mu 0 4 79 80 91 90
		f 4 73 164 -84 -164
		mu 0 4 80 81 92 91
		f 4 74 165 -85 -165
		mu 0 4 81 82 93 92
		f 4 75 166 -86 -166
		mu 0 4 82 83 94 93
		f 4 76 167 -87 -167
		mu 0 4 83 84 95 94
		f 4 77 168 -88 -168
		mu 0 4 84 85 96 95
		f 4 78 169 -89 -169
		mu 0 4 85 86 97 96
		f 4 79 160 -90 -170
		mu 0 4 86 87 98 97
		f 3 -1 -171 171
		mu 0 3 1 0 99
		f 3 -2 -172 172
		mu 0 3 2 1 100
		f 3 -3 -173 173
		mu 0 3 3 2 101
		f 3 -4 -174 174
		mu 0 3 4 3 102
		f 3 -5 -175 175
		mu 0 3 5 4 103
		f 3 -6 -176 176
		mu 0 3 6 5 104
		f 3 -7 -177 177
		mu 0 3 7 6 105
		f 3 -8 -178 178
		mu 0 3 8 7 106
		f 3 -9 -179 179
		mu 0 3 9 8 107
		f 3 -10 -180 170
		mu 0 3 10 9 108
		f 3 80 181 -181
		mu 0 3 88 89 109
		f 3 81 182 -182
		mu 0 3 89 90 110
		f 3 82 183 -183
		mu 0 3 90 91 111
		f 3 83 184 -184
		mu 0 3 91 92 112
		f 3 84 185 -185
		mu 0 3 92 93 113
		f 3 85 186 -186
		mu 0 3 93 94 114
		f 3 86 187 -187
		mu 0 3 94 95 115
		f 3 87 188 -188
		mu 0 3 95 96 116
		f 3 88 189 -189
		mu 0 3 96 97 117
		f 3 89 180 -190
		mu 0 3 97 98 118;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "F262822F-4280-E196-D5F5-A7A363C3C9F8";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "9677BCDD-445D-D76F-6D42-6AA57BAE53FF";
createNode displayLayer -n "defaultLayer";
	rename -uid "54155549-4B58-C95B-35D0-FEB8BA9D91F6";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "4C1EDE22-4FA1-0088-846D-0F93CFDB52BA";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "36EE07CF-45D5-AC0D-962A-DCB0E484649E";
	setAttr ".g" yes;
createNode polyCube -n "polyCube1";
	rename -uid "CC195E8D-4AAD-37BA-7961-C19D213A75DE";
	setAttr ".sw" 4;
	setAttr ".sh" 4;
	setAttr ".sd" 4;
	setAttr ".cuv" 4;
createNode polySoftEdge -n "polySoftEdge1";
	rename -uid "7F6FF1F1-422A-8DA3-D7C6-788080F3E340";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 14 "e[0:3]" "e[16:19]" "e[32:35]" "e[48:51]" "e[84]" "e[88:89]" "e[93:94]" "e[98:99]" "e[103]" "e[124]" "e[128:129]" "e[133:134]" "e[138:139]" "e[143]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".a" 180;
createNode polyTweak -n "polyTweak1";
	rename -uid "92E83E2B-4CEF-F9AD-EDBC-B985DBC50999";
	setAttr ".uopa" yes;
	setAttr -s 98 ".tk[0:97]" -type "float3"  0.222553 -0.56434226 -0.22255355
		 0.076382048 -0.5643428 -0.15276396 0 -0.56434268 -0.10296365 -0.076381773 -0.56434262
		 -0.15276396 -0.22255288 -0.56434226 -0.22255355 0.16924058 -0.28217098 -0.16924083
		 0.043020643 -0.28217149 -0.086041287 0 -0.28217146 -0.02667176 -0.043020509 -0.28217134
		 -0.086041287 -0.16924052 -0.28217098 -0.16924083 0.15060249 2.6720562e-007 -0.15060249
		 0.031357419 1.1940959e-007 -0.062714837 0 0 0 -0.031357419 5.8768663e-008 -0.062714837
		 -0.15060249 2.1013801e-007 -0.15060249 0.15060246 0.28217137 -0.15060222 0.031357359
		 0.28217134 -0.062714748 0 0.28217137 0 -0.031357475 0.28217137 -0.062714748 -0.15060252
		 0.28217122 -0.15060222 0.2018787 0.56434202 -0.2018782 0.063444458 0.5643428 -0.12688899
		 0 0.56434274 -0.073378116 -0.063444704 0.5643428 -0.12688899 -0.20187877 0.56434202
		 -0.2018782 0.12688904 0.56434274 -0.063444398 0.1102756 0.64025819 -0.10838518 2.7938806e-005
		 0.64009082 -0.063088849 -0.11221115 0.6398918 -0.10838532 -0.12688927 0.56434274
		 -0.063444398 0.073378116 0.56434274 0 0.063472502 0.64002365 0.00035541435 2.804482e-005
		 0.64002842 0.00035524782 -0.06341666 0.6400333 0.00035546048 -0.073378116 0.56434274
		 0 0.12688904 0.56434274 0.063444763 0.11027556 0.64015889 0.11410232 2.7892664e-005
		 0.63996613 0.063799717 -0.11221116 0.63979244 0.11410222 -0.12688927 0.56434274 0.063444763
		 0.2018787 0.56434202 0.20187931 0.063444458 0.56434268 0.12688936 0 0.56434274 0.073378116
		 -0.063444704 0.56434268 0.12688936 -0.20187877 0.56434202 0.20187931 0.15060246 0.2821708
		 0.15060276 0.031357359 0.28217137 0.062714942 0 0.28217137 0 -0.031357475 0.2821714
		 0.062714942 -0.15060252 0.28217074 0.15060276 0.15060249 -2.1013805e-007 0.15060249
		 0.031357419 -5.8768677e-008 0.062714837 0 0 0 -0.031357419 -1.1940959e-007 0.062714837
		 -0.15060249 -2.6720565e-007 0.15060249 0.16924058 -0.2821714 0.16924027 0.043020643
		 -0.2821714 0.086041093 0 -0.28217137 0.02667176 -0.043020509 -0.28217134 0.086041093
		 -0.16924052 -0.2821714 0.16924027 0.222553 -0.56434226 0.2225524 0.076382048 -0.5643428
		 0.15276356 0 -0.56434274 0.10296365 -0.076381773 -0.5643428 0.15276356 -0.22255288
		 -0.56434226 0.2225524 0.15276395 -0.56434274 0.076381713 0.11525015 -0.63989395 0.13384077
		 -2.793792e-005 -0.64008844 0.090638086 -0.11331468 -0.64025187 0.13384064 -0.15276368
		 -0.56434274 0.076381713 0.10296365 -0.56434274 0 0.067502573 -0.64003223 0.023107879
		 -2.8044004e-005 -0.64002573 0.026473174 -0.06755843 -0.64002258 0.023107925 -0.10296365
		 -0.56434274 0 0.15276395 -0.56434274 -0.076382093 0.11525013 -0.63979656 -0.094724797
		 -2.789278e-005 -0.63996643 -0.044422295 -0.11331464 -0.64015543 -0.094724938 -0.15276368
		 -0.56434274 -0.076382093 -0.0860411 -0.28217134 0.043020491 -0.02667176 -0.28217137
		 0 -0.0860411 -0.28217137 -0.043020673 -0.062714837 -1.0518546e-007 0.031357419 0
		 0 0 -0.062714837 -1.609637e-008 -0.031357419 -0.06271489 0.28217143 0.031357512 0
		 0.28217137 0 -0.06271489 0.28217134 -0.031357322 0.086041234 -0.28217137 0.043020491
		 0.02667176 -0.28217137 0 0.086041234 -0.2821714 -0.043020673 0.062714837 1.6096349e-008
		 0.031357419 0 0 0 0.062714837 1.0518546e-007 -0.031357419 0.062714778 0.2821714 0.031357512
		 0 0.28217137 0 0.062714778 0.28217131 -0.031357322;
createNode polySoftEdge -n "polySoftEdge2";
	rename -uid "61CA076D-4169-0AB2-0D5C-E9B460EEFFE5";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 10 "e[64]" "e[68:69]" "e[73:74]" "e[78:79]" "e[83]" "e[104]" "e[108:109]" "e[113:114]" "e[118:119]" "e[123]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".a" 180;
createNode polySplitRing -n "polySplitRing1";
	rename -uid "A6DF098B-469B-3FAB-F6F9-D18C9EB4DA87";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[69:73]" "e[114:118]" "e[159:161]" "e[183:185]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".wt" 0.5009760856628418;
	setAttr ".re" 72;
	setAttr ".sma" 29.999999999999996;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyExtrudeFace -n "polyExtrudeFace1";
	rename -uid "E04AA8A7-4873-05DC-4A1A-8CB379360911";
	setAttr ".ics" -type "componentList" 2 "f[96]" "f[105]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.68400699 19.935122 0 ;
	setAttr ".rs" 46128;
	setAttr ".lt" -type "double3" 1.3322676295501878e-015 -1.27675647831893e-015 7.3589813922278129 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 0 19.104316273172831 -3.1284254476869551 ;
	setAttr ".cbx" -type "double3" 1.3680140069788926 20.765925316648882 3.1284254476869551 ;
createNode polyExtrudeFace -n "polyExtrudeFace2";
	rename -uid "2E8488CD-447D-4A21-7ED7-4ABAAABFA14A";
	setAttr ".ics" -type "componentList" 2 "f[2]" "f[46]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.64751989 15.771349 0 ;
	setAttr ".rs" 54868;
	setAttr ".lt" -type "double3" 7.7715611723760958e-016 1.3877787807814457e-015 3.3388094323564346 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 0 14.106491140720873 -2.9615439905139906 ;
	setAttr ".cbx" -type "double3" 1.2950397658226651 17.436207298956745 2.9615439905139906 ;
createNode polyExtrudeFace -n "polyExtrudeFace3";
	rename -uid "10100C45-4B9E-BB9E-A47B-228EDD057C53";
	setAttr ".ics" -type "componentList" 2 "f[2]" "f[46]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 1.5594735 15.315561 -7.4587473e-007 ;
	setAttr ".rs" 34100;
	setAttr ".lt" -type "double3" 1.1102230246251565e-016 -1.9428902930940239e-016 1.4734380233802244 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 0.91195131514599648 13.650701998452561 -6.1408908667937885 ;
	setAttr ".cbx" -type "double3" 2.2069956494515055 16.980419648437937 6.1408893750442886 ;
createNode polyExtrudeFace -n "polyExtrudeFace4";
	rename -uid "D4F5F626-4A4C-8C1E-09BB-EF91A09CE766";
	setAttr ".ics" -type "componentList" 2 "f[2]" "f[46]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 1.9619243 13.376217 -1.1188122e-006 ;
	setAttr ".rs" 58884;
	setAttr ".lt" -type "double3" -5.5511151231257827e-017 -8.7152507433074788e-015 
		1.8957621927971167 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 1.3144010893504487 11.711357536063053 -7.543956977948846 ;
	setAttr ".cbx" -type "double3" 2.6094474748115206 15.041076677797928 7.5439547403245957 ;
createNode polyTweak -n "polyTweak2";
	rename -uid "99BFCEE5-4290-5B98-440A-338C5C6D090D";
	setAttr ".uopa" yes;
	setAttr -s 20 ".tk";
	setAttr ".tk[130]" -type "float3" 0 -0.27780771 0 ;
	setAttr ".tk[131]" -type "float3" 0 -0.27780771 0 ;
	setAttr ".tk[132]" -type "float3" 0 -0.27780771 0 ;
	setAttr ".tk[133]" -type "float3" 0 -0.27780771 0 ;
	setAttr ".tk[134]" -type "float3" 0 -0.27780771 0 ;
	setAttr ".tk[135]" -type "float3" 0 -0.27780771 0 ;
	setAttr ".tk[136]" -type "float3" 0 -0.27780771 0 ;
	setAttr ".tk[137]" -type "float3" 0 -0.27780771 0 ;
createNode polyExtrudeFace -n "polyExtrudeFace5";
	rename -uid "DC806641-47BD-F224-2DA9-948682E69AF5";
	setAttr ".ics" -type "componentList" 2 "f[18]" "f[30]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.58362424 27.662331 0 ;
	setAttr ".rs" 61068;
	setAttr ".lt" -type "double3" 7.2164496600635175e-016 -6.6613381477509392e-016 3.4426720810923981 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 0 27.425358757163369 -2.6693085852212395 ;
	setAttr ".cbx" -type "double3" 1.1672484854883614 27.899302490848207 2.6693085852212395 ;
createNode polyTweak -n "polyTweak3";
	rename -uid "1CA187D0-4558-A98C-7B36-79B0C92A4763";
	setAttr ".uopa" yes;
	setAttr -s 23 ".tk";
	setAttr ".tk[124]" -type "float3" 0 -0.03992435 0 ;
	setAttr ".tk[125]" -type "float3" 0 -0.03992435 0 ;
	setAttr ".tk[126]" -type "float3" 0 -0.03992435 0 ;
	setAttr ".tk[127]" -type "float3" 0 -0.03992435 0 ;
	setAttr ".tk[130]" -type "float3" 0 0.15119159 0 ;
	setAttr ".tk[131]" -type "float3" 0 0.15119159 0 ;
	setAttr ".tk[132]" -type "float3" 0 -0.085739501 0 ;
	setAttr ".tk[133]" -type "float3" 0 -0.085739501 0 ;
	setAttr ".tk[134]" -type "float3" 0 -0.059472799 0 ;
	setAttr ".tk[135]" -type "float3" 0 -0.059472799 0 ;
	setAttr ".tk[136]" -type "float3" 0 0.15119159 0 ;
	setAttr ".tk[137]" -type "float3" 0 0.15119159 0 ;
	setAttr ".tk[138]" -type "float3" 2.2351742e-008 0.27037585 0.080110535 ;
	setAttr ".tk[139]" -type "float3" 7.4505806e-009 0.27037585 0.077296928 ;
	setAttr ".tk[140]" -type "float3" -4.4703484e-008 -0.23726551 -5.3290705e-015 ;
	setAttr ".tk[141]" -type "float3" 0 -0.23726551 -8.8817842e-016 ;
	setAttr ".tk[142]" -type "float3" -1.4901161e-008 -0.23726627 -1.7763568e-015 ;
	setAttr ".tk[143]" -type "float3" -1.4901161e-008 -0.23726627 3.5527137e-015 ;
	setAttr ".tk[144]" -type "float3" -7.4505806e-009 0.27037555 -0.07729695 ;
	setAttr ".tk[145]" -type "float3" 7.4505806e-009 0.27037555 -0.080110535 ;
createNode polyExtrudeFace -n "polyExtrudeFace6";
	rename -uid "0C59B99C-4209-B072-2476-558EE8097EC9";
	setAttr ".ics" -type "componentList" 2 "f[18]" "f[30]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.90132445 30.937365 0.007587411 ;
	setAttr ".rs" 51262;
	setAttr ".lt" -type "double3" 6.2450045135165055e-017 7.7715611723760958e-016 2.9140111934152015 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 0.31239427870562503 30.698958282703529 -3.6729240845418469 ;
	setAttr ".cbx" -type "double3" 1.4902545806575735 31.175772142426606 3.6880989063318728 ;
createNode polyMergeVert -n "polyMergeVert1";
	rename -uid "A1CA5E61-4487-A0FE-6F86-4F91CA6BCE7E";
	setAttr ".ics" -type "componentList" 1 "vtx[158:161]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".d" 1;
	setAttr ".am" yes;
createNode polyMergeVert -n "polyMergeVert2";
	rename -uid "5F5F2935-4DC7-E94F-84A9-A8A6FFC94939";
	setAttr ".ics" -type "componentList" 1 "vtx[154:157]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".d" 1;
	setAttr ".am" yes;
createNode polySplitRing -n "polySplitRing2";
	rename -uid "7F3ECED4-4AA9-7E77-B21E-A79EAAE31655";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[232:233]" "e[235]" "e[237]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".wt" 0.30938011407852173;
	setAttr ".re" 235;
	setAttr ".sma" 29.999999999999996;
	setAttr ".stp" 2;
	setAttr ".div" 1;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak4";
	rename -uid "550FB232-4E9F-7665-9AEE-48827B598D81";
	setAttr ".uopa" yes;
	setAttr -s 36 ".tk";
	setAttr ".tk[114]" -type "float3" -0.059816986 0.076740094 0.021486368 ;
	setAttr ".tk[115]" -type "float3" 0.063180722 0.076740094 -0.013794231 ;
	setAttr ".tk[116]" -type "float3" -0.06318073 -0.076740101 0.014758838 ;
	setAttr ".tk[117]" -type "float3" 0.063180722 -0.076740101 -0.021486368 ;
	setAttr ".tk[118]" -type "float3" 0.069878541 -0.084875435 0.023763988 ;
	setAttr ".tk[119]" -type "float3" -0.069878541 -0.084875435 -0.016323637 ;
	setAttr ".tk[120]" -type "float3" -0.066158347 0.084875427 -0.023763992 ;
	setAttr ".tk[121]" -type "float3" 0.069878541 0.084875427 0.01525639 ;
	setAttr ".tk[123]" -type "float3" -0.074150845 0 0 ;
	setAttr ".tk[124]" -type "float3" -0.074150845 0 0 ;
	setAttr ".tk[127]" -type "float3" -0.07415086 0 0 ;
	setAttr ".tk[128]" -type "float3" -0.07415086 0 0 ;
	setAttr ".tk[131]" -type "float3" -0.14248577 0 0 ;
	setAttr ".tk[132]" -type "float3" -0.14248577 0 0 ;
	setAttr ".tk[135]" -type "float3" -0.14248577 0 0 ;
	setAttr ".tk[136]" -type "float3" -0.14248577 0 0 ;
	setAttr ".tk[139]" -type "float3" -0.14248577 0 0.057900965 ;
	setAttr ".tk[140]" -type "float3" -0.17584784 0 0.057900965 ;
	setAttr ".tk[143]" -type "float3" -0.17584784 0 -0.054081857 ;
	setAttr ".tk[144]" -type "float3" -0.14248577 0 -0.054081857 ;
	setAttr ".tk[146]" -type "float3" 0.041820101 0.016980421 -0.063890018 ;
	setAttr ".tk[147]" -type "float3" -0.041820098 0.016980421 -0.039898992 ;
	setAttr ".tk[148]" -type "float3" -0.019956153 -0.016891271 0.063890018 ;
	setAttr ".tk[149]" -type "float3" 0.04180757 -0.016980421 0.043581791 ;
	setAttr ".tk[150]" -type "float3" 0.045131531 -0.018300232 -0.046007421 ;
	setAttr ".tk[151]" -type "float3" -0.021542763 -0.01821645 -0.070353128 ;
	setAttr ".tk[152]" -type "float3" -0.045145035 0.018300232 0.04445444 ;
	setAttr ".tk[153]" -type "float3" 0.045145035 0.018300232 0.070353121 ;
	setAttr ".tk[154]" -type "float3" -1.4901161e-008 -1.8626451e-009 7.4505806e-009 ;
	setAttr ".tk[155]" -type "float3" 1.4901161e-008 -1.8626451e-009 -3.7252903e-009 ;
	setAttr ".tk[156]" -type "float3" -3.7252903e-009 1.8626451e-009 1.4901161e-008 ;
	setAttr ".tk[157]" -type "float3" 1.4901161e-008 1.8626451e-009 3.7252903e-008 ;
	setAttr ".tk[158]" -type "float3" -8.1956387e-008 -9.3132257e-009 4.4703484e-008 ;
	setAttr ".tk[159]" -type "float3" 4.0978193e-008 -1.8626451e-009 -1.0430813e-007 ;
	setAttr ".tk[160]" -type "float3" -2.9802322e-008 9.3132257e-009 -7.4505806e-009 ;
	setAttr ".tk[161]" -type "float3" 2.2351742e-008 9.3132257e-009 1.1175871e-007 ;
createNode polySplitRing -n "polySplitRing3";
	rename -uid "CD5972F0-4F92-24C5-7A4C-41AE3C448B17";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[224:225]" "e[227]" "e[229]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".wt" 0.44452685117721558;
	setAttr ".dr" no;
	setAttr ".re" 225;
	setAttr ".sma" 29.999999999999996;
	setAttr ".stp" 2;
	setAttr ".div" 1;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyExtrudeFace -n "polyExtrudeFace7";
	rename -uid "A103E623-40EA-D46F-9D00-E98C42AD5D5E";
	setAttr ".ics" -type "componentList" 2 "f[94]" "f[103]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 2.7106662 19.581001 0.0071253413 ;
	setAttr ".rs" 43206;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 2.4219712969772003 19.230349713189927 -10.045277041399341 ;
	setAttr ".cbx" -type "double3" 2.9993611456229816 19.931654624328278 10.059527724374039 ;
createNode polyTweak -n "polyTweak5";
	rename -uid "9F5A75CF-4235-154F-1A75-6EAB65E2FF28";
	setAttr ".uopa" yes;
	setAttr -s 10 ".tk";
	setAttr ".tk[156]" -type "float3" -0.022951642 0.029444983 -0.0082442081 ;
	setAttr ".tk[157]" -type "float3" -0.024242263 -0.029444983 -0.005662946 ;
	setAttr ".tk[158]" -type "float3" 0.024242263 -0.029444983 0.0082442081 ;
	setAttr ".tk[159]" -type "float3" 0.024242263 0.029444983 0.0052927206 ;
	setAttr ".tk[160]" -type "float3" 0.023651492 0.028727408 -0.0051637739 ;
	setAttr ".tk[161]" -type "float3" 0.023651492 -0.028727408 -0.0080433534 ;
	setAttr ".tk[162]" -type "float3" -0.023651484 -0.028727408 0.0055248914 ;
	setAttr ".tk[163]" -type "float3" -0.022392299 0.028727408 0.0080433534 ;
createNode polySplitRing -n "polySplitRing4";
	rename -uid "42C9A6BC-4C73-CB83-34F7-F7842B3E071E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[232:233]" "e[235]" "e[237]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".wt" 0.099118441343307495;
	setAttr ".re" 235;
	setAttr ".sma" 29.999999999999996;
	setAttr ".div" 1;
	setAttr ".p[0]"  0 0 1;
	setAttr ".uem" no;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak6";
	rename -uid "FC68F062-48A4-1B4A-ADE3-1F8F0B9D69FA";
	setAttr ".uopa" yes;
	setAttr -s 14 ".tk";
	setAttr ".tk[164]" -type "float3" 0.0030558063 -0.0039203456 0.11019193 ;
	setAttr ".tk[165]" -type "float3" -0.0032276481 -0.0039203456 0.11199427 ;
	setAttr ".tk[166]" -type "float3" 0.0032276481 0.0039203456 0.11053557 ;
	setAttr ".tk[167]" -type "float3" -0.0032276481 0.0039203456 0.11238723 ;
	setAttr ".tk[168]" -type "float3" -0.0027591365 0.0033513117 -0.11238723 ;
	setAttr ".tk[169]" -type "float3" 0.0027590822 0.0033513117 -0.11080436 ;
	setAttr ".tk[170]" -type "float3" 0.0026121926 -0.0033511943 -0.11051064 ;
	setAttr ".tk[171]" -type "float3" -0.0027591365 -0.0033511943 -0.11205129 ;
createNode polySplitRing -n "polySplitRing5";
	rename -uid "CCB6B96F-41E5-E2E4-F69A-5B91F8983A64";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[224:225]" "e[227]" "e[229]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".wt" 0.071292176842689514;
	setAttr ".re" 225;
	setAttr ".sma" 29.999999999999996;
	setAttr ".div" 1;
	setAttr ".p[0]"  0 0 1;
	setAttr ".uem" no;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing6";
	rename -uid "AF6C2744-4864-9131-7F5C-96A9E99F2E35";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[248:249]" "e[251]" "e[253]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".wt" 0.066444128751754761;
	setAttr ".re" 251;
	setAttr ".sma" 29.999999999999996;
	setAttr ".div" 1;
	setAttr ".p[0]"  0 0 1;
	setAttr ".uem" no;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing7";
	rename -uid "E5218B23-4C8D-31BD-F5D1-0FBDA5EA7F87";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[240:241]" "e[243]" "e[245]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".wt" 0.066079065203666687;
	setAttr ".re" 241;
	setAttr ".sma" 29.999999999999996;
	setAttr ".div" 1;
	setAttr ".p[0]"  0 0 1;
	setAttr ".uem" no;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing8";
	rename -uid "54085E21-49C9-28F7-7DF9-F0BACFEB18A7";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[296:297]" "e[299]" "e[301]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".wt" 0.045353591442108154;
	setAttr ".re" 299;
	setAttr ".sma" 29.999999999999996;
	setAttr ".div" 1;
	setAttr ".p[0]"  0 0 1;
	setAttr ".uem" no;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing9";
	rename -uid "26B2733F-4A35-13B6-AF5D-66B0E06FF6DF";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[288:289]" "e[291]" "e[293]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".wt" 0.054625336080789566;
	setAttr ".re" 291;
	setAttr ".sma" 29.999999999999996;
	setAttr ".div" 1;
	setAttr ".p[0]"  0 0 1;
	setAttr ".uem" no;
	setAttr ".fq" yes;
createNode polyTweakUV -n "polyTweakUV1";
	rename -uid "34B05CF2-4F4D-474E-B356-51B403223CCA";
	setAttr ".uopa" yes;
	setAttr -s 5 ".uvtk";
	setAttr ".uvtk[39]" -type "float2" 0.0044787335 0.0072386893 ;
	setAttr ".uvtk[44]" -type "float2" 0.018233271 -0.041031614 ;
	setAttr ".uvtk[101]" -type "float2" -0.059713446 -1.0664045e-005 ;
	setAttr ".uvtk[102]" -type "float2" 0.00035048029 -1.7709064e-005 ;
createNode polyMergeVert -n "polyMergeVert3";
	rename -uid "7C6DE7B6-4947-EC3F-0A8A-6FB417F1437F";
	setAttr ".ics" -type "componentList" 2 "vtx[39]" "vtx[44]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak7";
	rename -uid "7900F811-4F3A-BDD8-9386-6698C36F6A50";
	setAttr ".uopa" yes;
	setAttr -s 38 ".tk";
	setAttr ".tk[44]" -type "float3" 0.074989587 9.5367432e-007 0.11156547 ;
	setAttr ".tk[114]" -type "float3" 0 0 -0.23790738 ;
	setAttr ".tk[115]" -type "float3" 0 0 -0.24179873 ;
	setAttr ".tk[116]" -type "float3" 0 0 -0.2386494 ;
	setAttr ".tk[117]" -type "float3" 0 0 -0.24264711 ;
	setAttr ".tk[118]" -type "float3" 0 0 0.24264711 ;
	setAttr ".tk[119]" -type "float3" 0 0 0.23922974 ;
	setAttr ".tk[120]" -type "float3" 0 0 0.23859546 ;
	setAttr ".tk[121]" -type "float3" 0 0 0.24192187 ;
	setAttr ".tk[156]" -type "float3" 0 0 0.15277123 ;
	setAttr ".tk[157]" -type "float3" 0 0 0.15357754 ;
	setAttr ".tk[158]" -type "float3" 0 0 0.15792173 ;
	setAttr ".tk[159]" -type "float3" 0 0 0.15699977 ;
	setAttr ".tk[160]" -type "float3" 0 0 -0.15678568 ;
	setAttr ".tk[161]" -type "float3" 0 0 -0.15778005 ;
	setAttr ".tk[162]" -type "float3" 0 0 -0.15309455 ;
	setAttr ".tk[163]" -type "float3" 0 0 -0.15222491 ;
	setAttr ".tk[164]" -type "float3" 0 0 -0.25454959 ;
	setAttr ".tk[165]" -type "float3" 0 0 -0.25871313 ;
	setAttr ".tk[166]" -type "float3" 0 0 -0.25534353 ;
	setAttr ".tk[167]" -type "float3" 0 0 -0.25962088 ;
	setAttr ".tk[168]" -type "float3" 0 0 0.25962088 ;
	setAttr ".tk[169]" -type "float3" 0 0 0.25596443 ;
	setAttr ".tk[170]" -type "float3" 0 0 0.2552858 ;
	setAttr ".tk[171]" -type "float3" 0 0 0.25884488 ;
	setAttr ".tk[172]" -type "float3" -0.018016666 0.022519026 0 ;
	setAttr ".tk[173]" -type "float3" -0.018997578 -0.022238566 0 ;
	setAttr ".tk[174]" -type "float3" 0.017851682 -0.022238566 0 ;
	setAttr ".tk[175]" -type "float3" 0.017851682 0.022519026 0 ;
	setAttr ".tk[176]" -type "float3" 0.018997578 0.02279578 1.8626451e-009 ;
	setAttr ".tk[177]" -type "float3" 0.018997578 -0.022795783 0 ;
	setAttr ".tk[178]" -type "float3" -0.018538296 -0.022795783 0 ;
	setAttr ".tk[179]" -type "float3" -0.017539101 0.02279578 0 ;
createNode polyTweakUV -n "polyTweakUV2";
	rename -uid "F5C022BD-4406-66A8-9844-FCBB0BAAD24E";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[24]" -type "float2" 0.037734035 0.020513764 ;
	setAttr ".uvtk[29]" -type "float2" 0.0044440646 -0.0071914024 ;
	setAttr ".uvtk[102]" -type "float2" -0.00035048142 -1.7709055e-005 ;
createNode polyMergeVert -n "polyMergeVert4";
	rename -uid "1A7C11BD-42BE-BD77-5A31-98BA0AD44323";
	setAttr ".ics" -type "componentList" 2 "vtx[24]" "vtx[29]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak8";
	rename -uid "B2973676-4455-ED78-ED76-5B932B2BEC65";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[24]" -type "float3" 0.074989587 9.5367432e-007 -0.11156623 ;
createNode polyTweakUV -n "polyTweakUV3";
	rename -uid "7BBFB758-4B93-56DC-ACD8-B0B791957C8A";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[19]" -type "float2" 0.058016006 0.0068874005 ;
	setAttr ".uvtk[98]" -type "float2" -0.0029053334 0.0011962554 ;
createNode polyMergeVert -n "polyMergeVert5";
	rename -uid "EB26B249-4AC3-0D1C-6D13-A39DDE1A765B";
	setAttr ".ics" -type "componentList" 2 "vtx[19]" "vtx[86]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak9";
	rename -uid "AADD35AC-4C96-F4B6-B196-8CAE5BF1C80C";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[19]" -type "float3" 0.087887675 7.1525574e-007 -0.13075511 ;
createNode polyTweakUV -n "polyTweakUV4";
	rename -uid "6776BB08-4885-D387-732F-CCAC29C89B8E";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[47]" -type "float2" 0.039159134 -0.028564004 ;
	setAttr ".uvtk[95]" -type "float2" -0.062442306 -5.419708e-006 ;
	setAttr ".uvtk[96]" -type "float2" 0.0016639216 -3.7238078e-006 ;
createNode polyMergeVert -n "polyMergeVert6";
	rename -uid "63503EE9-4BCB-4FAA-FC2D-74A11404B35C";
	setAttr ".ics" -type "componentList" 2 "vtx[47]" "vtx[84]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak10";
	rename -uid "7444A18B-4B67-7EBA-7F4D-17BF53CD1421";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[47]" -type "float3" 0.087887675 7.1525574e-007 0.13075484 ;
createNode polyTweakUV -n "polyTweakUV5";
	rename -uid "F4029B14-4B5B-66B5-6F3C-71B3FEC2B75B";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[52]" -type "float2" 0.043027844 -0.022911595 ;
	setAttr ".uvtk[91]" -type "float2" -0.062582508 5.6736794e-006 ;
	setAttr ".uvtk[92]" -type "float2" -0.00075295224 9.5343637e-007 ;
createNode polyMergeVert -n "polyMergeVert7";
	rename -uid "357DF104-4025-17DF-DB8E-8FA5270E6914";
	setAttr ".ics" -type "componentList" 2 "vtx[52]" "vtx[81]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak11";
	rename -uid "4629CC6C-46C9-5BAB-77CD-31AD9B0EC2AC";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[52]" -type "float3" 0.087887555 7.1525574e-007 0.13075498 ;
	setAttr ".tk[81]" -type "float3" 0 -4.4408921e-016 0 ;
createNode polyTweakUV -n "polyTweakUV6";
	rename -uid "2668A6B4-402B-22E8-F870-0BB2B70BE135";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[14]" -type "float2" 0.061779086 0.0011438191 ;
	setAttr ".uvtk[93]" -type "float2" 0.00027601252 0.00068198197 ;
createNode polyMergeVert -n "polyMergeVert8";
	rename -uid "7F418982-4727-42B8-10E1-2EA1CBE41CFC";
	setAttr ".ics" -type "componentList" 2 "vtx[14]" "vtx[82]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak12";
	rename -uid "40F41D5B-4206-93CD-58F1-62AE53F3A718";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[14]" -type "float3" 0.087887555 -2.3841858e-007 -0.13075498 ;
	setAttr ".tk[82]" -type "float3" 0 -4.4408921e-016 0 ;
createNode polyTweakUV -n "polyTweakUV7";
	rename -uid "9A24D6E8-4A66-1EF7-6F97-B89A8604E374";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[133]" -type "float2" -0.0002348422 0.00023063878 ;
	setAttr ".uvtk[134]" -type "float2" 0.062075883 0.00031974458 ;
createNode polyMergeVert -n "polyMergeVert9";
	rename -uid "68E80AC2-4C0C-7B0D-360C-BBB2AF952803";
	setAttr ".ics" -type "componentList" 1 "vtx[106:107]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak13";
	rename -uid "C5F4A68D-4F51-71E2-7F55-868F6B84A837";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[107]" -type "float3"  0.085548043 -2.3841858e-007 -0.12727416;
createNode polyTweakUV -n "polyTweakUV8";
	rename -uid "9B71F04B-4985-761D-F56F-D38926A7072F";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[129]" -type "float2" 0.041412044 -0.02137663 ;
	setAttr ".uvtk[130]" -type "float2" -0.062532589 8.6600426e-007 ;
	setAttr ".uvtk[131]" -type "float2" 7.4786592e-005 3.2176224e-006 ;
createNode polyMergeVert -n "polyMergeVert10";
	rename -uid "85E6340D-4CB8-A585-21D0-C9AEB7F7CA27";
	setAttr ".ics" -type "componentList" 1 "vtx[103:104]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak14";
	rename -uid "A82BBEB5-48A3-675A-6CA6-5CAAA8F85D04";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[103:104]" -type "float3"  0.085548043 4.7683716e-007
		 0.12727447 0 0 0;
createNode polyTweakUV -n "polyTweakUV9";
	rename -uid "FF8DAC8A-4BAE-D9C9-D402-3888246BBB6C";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[57]" -type "float2" 0.04567682 -0.015056752 ;
	setAttr ".uvtk[87]" -type "float2" -0.063749127 2.4410323e-005 ;
	setAttr ".uvtk[88]" -type "float2" -0.0012041309 8.1933185e-006 ;
createNode polyMergeVert -n "polyMergeVert11";
	rename -uid "BDEEE49F-4615-CAE6-5629-EE9C7B1BF1AE";
	setAttr ".ics" -type "componentList" 2 "vtx[57]" "vtx[78]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak15";
	rename -uid "81D6C0FE-45BA-21E6-3EDD-31BEA723D033";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[57]" -type "float3" 0.083199471 0 0.12378016 ;
createNode polyTweakUV -n "polyTweakUV10";
	rename -uid "216DA6A6-4F24-F1CC-4BF5-B3B07805014F";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[9]" -type "float2" 0.063426338 0.00010376156 ;
	setAttr ".uvtk[89]" -type "float2" 0.0011490957 5.6961984e-005 ;
createNode polyMergeVert -n "polyMergeVert12";
	rename -uid "831D9E25-4B24-6880-337F-F2A91A0656FA";
	setAttr ".ics" -type "componentList" 2 "vtx[9]" "vtx[79]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak16";
	rename -uid "B2A76B35-4969-0A61-255A-7987EF3AFFDE";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[9]" -type "float3" 0.083199471 -4.7683716e-007 -0.12377983 ;
createNode polyTweakUV -n "polyTweakUV11";
	rename -uid "B9355FD3-453C-9EAE-102E-8F80F275FFFD";
	setAttr ".uopa" yes;
	setAttr -s 5 ".uvtk";
	setAttr ".uvtk[62]" -type "float2" 0.021326933 0.026099917 ;
	setAttr ".uvtk[67]" -type "float2" 0.004315136 -0.0068828487 ;
	setAttr ".uvtk[83]" -type "float2" -0.062104564 2.3131055e-005 ;
	setAttr ".uvtk[84]" -type "float2" -0.0012920069 2.9511157e-005 ;
createNode polyMergeVert -n "polyMergeVert13";
	rename -uid "45B9827A-42FC-0DFF-8F04-78AE1CE3386B";
	setAttr ".ics" -type "componentList" 2 "vtx[62]" "vtx[67]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak17";
	rename -uid "C95F8C42-4799-62A5-0879-159819407E26";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[62]" -type "float3" 0.069789261 -4.7683716e-007 0.10382931 ;
createNode polyTweakUV -n "polyTweakUV12";
	rename -uid "6F46A898-467B-61C7-4C7E-71A775017B9C";
	setAttr ".uopa" yes;
	setAttr -s 5 ".uvtk";
	setAttr ".uvtk[4]" -type "float2" 0.056126669 6.1204564e-006 ;
	setAttr ".uvtk[76]" -type "float2" 0.004604483 0.0073603643 ;
	setAttr ".uvtk[81]" -type "float2" 0.0056100925 -0.056857266 ;
	setAttr ".uvtk[84]" -type "float2" 0.0012640394 3.8996368e-005 ;
createNode polyMergeVert -n "polyMergeVert14";
	rename -uid "5D14D99D-4FCA-C561-9777-EB9D884021A8";
	setAttr ".ics" -type "componentList" 2 "vtx[4]" "vtx[76]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak18";
	rename -uid "475C32E8-4B9B-F57D-E337-82B6D2D3F1E4";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[4]" -type "float3" 0.069789261 -4.7683716e-007 -0.10382855 ;
createNode polyTweakUV -n "polyTweakUV13";
	rename -uid "26D17318-482B-C953-3D2E-B9B2DBD11D91";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[0]" -type "float2" -0.056509305 -7.3932565e-006 ;
	setAttr ".uvtk[72]" -type "float2" -0.0046716114 0.0073239305 ;
	setAttr ".uvtk[77]" -type "float2" -0.0056630825 -0.056822758 ;
	setAttr ".uvtk[94]" -type "float2" 0.00038950387 1.8757131e-005 ;
createNode polyMergeVert -n "polyMergeVert15";
	rename -uid "5695F7DC-4012-0011-B198-3AA764A9E480";
	setAttr ".ics" -type "componentList" 2 "vtx[0]" "vtx[72]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak19";
	rename -uid "E9D6C000-46C2-23B7-7DA5-7983FF0F611B";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[0]" -type "float3" -0.069788963 -4.7683716e-007 -0.10382855 ;
createNode polyTweakUV -n "polyTweakUV14";
	rename -uid "CD0205B6-4AFB-CAF3-5516-2EBBD90F0F94";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[5]" -type "float2" -0.064517729 -1.0730756e-006 ;
	setAttr ".uvtk[96]" -type "float2" 0.00035233356 7.5198423e-006 ;
createNode polyMergeVert -n "polyMergeVert16";
	rename -uid "51CAA2DA-47EC-0465-01FE-B88BAA73DD19";
	setAttr ".ics" -type "componentList" 2 "vtx[5]" "vtx[80]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak20";
	rename -uid "D9831314-44D2-69A6-BEC9-B58F61C9A10A";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[5]" -type "float3" -0.083199322 -4.7683716e-007 -0.12377983 ;
createNode polyTweakUV -n "polyTweakUV15";
	rename -uid "38F9568B-478B-16B0-C0E2-7B956B60A70F";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[111]" -type "float2" -0.063031569 -2.3487587e-006 ;
	setAttr ".uvtk[112]" -type "float2" -3.7585429e-005 3.2026574e-006 ;
createNode polyMergeVert -n "polyMergeVert17";
	rename -uid "C58694C3-4B4C-D231-C231-7B99920EBC85";
	setAttr ".ics" -type "componentList" 1 "vtx[89:90]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak21";
	rename -uid "40645C89-496B-EC7C-766C-83861803228B";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[89:90]" -type "float3"  -0.085548162 -2.3841858e-007
		 -0.12727416 0 0 0;
createNode polyTweakUV -n "polyTweakUV16";
	rename -uid "74149872-4A29-E164-E3EF-8AB0A8C98446";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[10]" -type "float2" -0.063243128 2.4872114e-007 ;
	setAttr ".uvtk[99]" -type "float2" -0.00090889598 1.3695206e-006 ;
createNode polyMergeVert -n "polyMergeVert18";
	rename -uid "AF4281C6-4FF2-0197-96D4-DEACBCB15246";
	setAttr ".ics" -type "componentList" 2 "vtx[10]" "vtx[82]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak22";
	rename -uid "5A9601E4-475F-5365-FA67-BAAEF0739642";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[10]" -type "float3" -0.087887555 -2.3841858e-007 -0.13075498 ;
	setAttr ".tk[82]" -type "float3" 0 -4.4408921e-016 0 ;
createNode polyTweakUV -n "polyTweakUV17";
	rename -uid "A3DE301F-468B-BC82-8899-D192324BF85B";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[15]" -type "float2" -0.064503931 -5.329818e-007 ;
	setAttr ".uvtk[102]" -type "float2" 0.00075045263 -3.6792269e-006 ;
createNode polyMergeVert -n "polyMergeVert19";
	rename -uid "8EAEF576-42F4-E215-7E1F-72A705688986";
	setAttr ".ics" -type "componentList" 2 "vtx[15]" "vtx[84]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak23";
	rename -uid "C046CA8C-4401-C2FD-533C-98A82176E63C";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[15]" -type "float3" -0.087887555 -7.1525574e-007 -0.13075511 ;
createNode polyTweakUV -n "polyTweakUV18";
	rename -uid "4A3DC06C-406C-992A-A520-EDAACEA88E89";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[20]" -type "float2" -0.038444873 0.019249499 ;
	setAttr ".uvtk[25]" -type "float2" -0.0043734359 -0.0072268089 ;
	setAttr ".uvtk[105]" -type "float2" -0.0011196703 -2.5753212e-005 ;
createNode polyMergeVert -n "polyMergeVert20";
	rename -uid "99AA52D2-44FB-A0C1-4628-FB84A59AE34C";
	setAttr ".ics" -type "componentList" 2 "vtx[20]" "vtx[25]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak24";
	rename -uid "A8875949-47FE-3CC0-74C6-0CA8BDD00A6B";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[20]" -type "float3" -0.074989647 9.5367432e-007 -0.11156623 ;
createNode polyTweakUV -n "polyTweakUV19";
	rename -uid "5DB32333-4040-E261-7ECC-EC860C7DE0F5";
	setAttr ".uopa" yes;
	setAttr -s 5 ".uvtk";
	setAttr ".uvtk[57]" -type "float2" -0.017877841 0.040470853 ;
	setAttr ".uvtk[62]" -type "float2" -0.0043924171 -0.0068657799 ;
	setAttr ".uvtk[89]" -type "float2" 0.059395056 1.0627263e-005 ;
	setAttr ".uvtk[90]" -type "float2" -0.00038950233 1.8750876e-005 ;
createNode polyMergeVert -n "polyMergeVert21";
	rename -uid "2436F8C1-4456-E156-39DB-34B575009E2F";
	setAttr ".ics" -type "componentList" 2 "vtx[57]" "vtx[62]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak25";
	rename -uid "A45A2F4F-48B8-CA8F-4FBB-229AD8D694EE";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[57]" -type "float3" -0.069788963 -4.7683716e-007 0.10382931 ;
createNode polyTweakUV -n "polyTweakUV20";
	rename -uid "93E29F3D-4DA7-80A3-1B6D-6C8BD1ADDF4A";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[52]" -type "float2" -0.035279714 0.036142226 ;
	setAttr ".uvtk[90]" -type "float2" 0.062204748 1.7547491e-005 ;
	setAttr ".uvtk[91]" -type "float2" -0.00018438026 8.5643569e-006 ;
createNode polyMergeVert -n "polyMergeVert22";
	rename -uid "3852E285-4CBF-0B91-87FC-A1933F53C470";
	setAttr ".ics" -type "componentList" 2 "vtx[52]" "vtx[76]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak26";
	rename -uid "7A57A176-4680-5F34-8911-208A0A9FD1D5";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[52]" -type "float3" -0.083199322 0 0.12378016 ;
createNode polyTweakUV -n "polyTweakUV21";
	rename -uid "A7E1D64F-46C1-FF47-A804-489DAD8E8026";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[106]" -type "float2" -0.00022423128 5.6042427e-006 ;
	setAttr ".uvtk[107]" -type "float2" 0.062369857 3.2797998e-006 ;
	setAttr ".uvtk[108]" -type "float2" -0.038522583 0.035478324 ;
createNode polyMergeVert -n "polyMergeVert23";
	rename -uid "766B1659-4D55-FE8B-04B4-DFB68F7DC636";
	setAttr ".ics" -type "componentList" 1 "vtx[86:87]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak27";
	rename -uid "8F4FEBEC-459F-DD62-5CAE-96A35ABB8323";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[87]" -type "float3"  -0.085548162 4.7683716e-007 0.12727447;
createNode polyTweakUV -n "polyTweakUV22";
	rename -uid "30689671-45B8-DB74-91F8-169CE2A490AE";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[47]" -type "float2" -0.043788776 0.029767174 ;
	setAttr ".uvtk[92]" -type "float2" 0.06250269 4.0839509e-006 ;
	setAttr ".uvtk[93]" -type "float2" 0.00079838879 2.3078528e-006 ;
createNode polyMergeVert -n "polyMergeVert24";
	rename -uid "F541540D-43E5-BAAC-7CE3-ECBFF162F14B";
	setAttr ".ics" -type "componentList" 2 "vtx[47]" "vtx[77]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak28";
	rename -uid "A668466B-406E-EF19-2CD1-CCA9771FD847";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[47]" -type "float3" -0.087887555 7.1525574e-007 0.13075498 ;
	setAttr ".tk[77]" -type "float3" 0 -4.4408921e-016 0 ;
createNode polyTweakUV -n "polyTweakUV23";
	rename -uid "64CE8A23-4E2B-E9DB-79B4-9F99CAB44835";
	setAttr ".uopa" yes;
	setAttr -s 4 ".uvtk";
	setAttr ".uvtk[42]" -type "float2" -0.05165258 0.016836235 ;
	setAttr ".uvtk[94]" -type "float2" 0.063813172 -9.0287167e-006 ;
	setAttr ".uvtk[95]" -type "float2" -0.00077405426 -3.5188464e-006 ;
createNode polyMergeVert -n "polyMergeVert25";
	rename -uid "3E1ABC1E-444E-CDFB-4BB1-618D1371348D";
	setAttr ".ics" -type "componentList" 2 "vtx[42]" "vtx[78]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak29";
	rename -uid "877203E6-4C77-613D-747C-34B773E1EF1A";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[42]" -type "float3" -0.087887555 7.1525574e-007 0.13075484 ;
createNode polyTweakUV -n "polyTweakUV24";
	rename -uid "C3330212-466B-822B-AC69-FA9BB3579807";
	setAttr ".uopa" yes;
	setAttr -s 5 ".uvtk";
	setAttr ".uvtk[33]" -type "float2" -0.0044212616 0.0072922772 ;
	setAttr ".uvtk[38]" -type "float2" -0.023369042 -0.026062602 ;
	setAttr ".uvtk[96]" -type "float2" 0.062156588 -2.3009694e-005 ;
	setAttr ".uvtk[97]" -type "float2" 0.0010659733 -2.6390782e-005 ;
createNode polyMergeVert -n "polyMergeVert26";
	rename -uid "A94D561A-44C2-0D39-9872-73AA4144C7F3";
	setAttr ".ics" -type "componentList" 2 "vtx[33]" "vtx[38]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak30";
	rename -uid "E67B880B-4C68-1AC3-FA70-9DBF5FD36E98";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[38]" -type "float3" -0.074989647 9.5367432e-007 0.11156547 ;
createNode polyBevel3 -n "polyBevel1";
	rename -uid "094D3C70-4599-8FFC-DA36-0AA3908FC3CB";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[213]" "e[223]" "e[229]" "e[239]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".at" 180;
	setAttr ".fn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
	setAttr ".ma" 180;
createNode polyTweakUV -n "polyTweakUV25";
	rename -uid "75DA0F80-4C86-229D-1C6F-A4B3996A18D4";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[178]" -type "float2" 0.0010965936 0.00030569528 ;
	setAttr ".uvtk[200]" -type "float2" 0.0033665395 0.00063784985 ;
createNode polyMergeVert -n "polyMergeVert27";
	rename -uid "D338CA60-41A7-45E7-6C0F-219B322FCB21";
	setAttr ".ics" -type "componentList" 2 "vtx[150]" "vtx[171]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak31";
	rename -uid "96E97E31-478F-6A25-8C1F-E9AC4FBB15D0";
	setAttr ".uopa" yes;
	setAttr -s 5 ".tk";
	setAttr ".tk[171]" -type "float3" -0.065564275 0.066704035 0.46529391 ;
createNode polyTweakUV -n "polyTweakUV26";
	rename -uid "6055E323-4CBA-ECE6-05B1-D1A9FAEEFCC0";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[175]" -type "float2" 0.001097423 -0.00030587934 ;
	setAttr ".uvtk[199]" -type "float2" 0.0033670606 -0.00063813262 ;
createNode polyMergeVert -n "polyMergeVert28";
	rename -uid "FF15593C-42BC-54F9-D3C6-239490CD9724";
	setAttr ".ics" -type "componentList" 2 "vtx[154]" "vtx[166]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak32";
	rename -uid "1A951DE4-4CE8-CB02-E2EC-E79A7F8ABB59";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[166]" -type "float3" -0.065591097 0.066730738 -0.46547964 ;
createNode polyTweakUV -n "polyTweakUV27";
	rename -uid "2734DBF2-4C28-68DD-AD67-4A91115F6249";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[180]" -type "float2" -0.012109943 0.018804319 ;
	setAttr ".uvtk[185]" -type "float2" -0.016398363 -0.0043749963 ;
createNode polyMergeVert -n "polyMergeVert29";
	rename -uid "06A353B5-438F-9A0D-061E-A4814D982E99";
	setAttr ".ics" -type "componentList" 2 "vtx[114]" "vtx[177]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak33";
	rename -uid "BF42B1F5-42F6-D825-C5F6-7EB6EA904E24";
	setAttr ".uopa" yes;
	setAttr -s 15 ".tk";
	setAttr ".tk[98]" -type "float3" -0.068751357 0 0 ;
	setAttr ".tk[103]" -type "float3" -0.068751357 0 0 ;
	setAttr ".tk[104]" -type "float3" -0.068751357 0 0 ;
	setAttr ".tk[109]" -type "float3" -0.068751357 0 0 ;
	setAttr ".tk[110]" -type "float3" 1.8626451e-009 0 0 ;
	setAttr ".tk[115]" -type "float3" 1.8626451e-009 0 0 ;
	setAttr ".tk[166]" -type "float3" -0.054711837 0.062339179 0 ;
	setAttr ".tk[169]" -type "float3" -0.054711837 0.062339179 0 ;
	setAttr ".tk[171]" -type "float3" -0.081212796 0.062339172 0 ;
	setAttr ".tk[172]" -type "float3" -0.081212796 0.062339172 0 ;
	setAttr ".tk[177]" -type "float3" -3.8743019e-007 0.012410641 0.0053501129 ;
createNode polyTweakUV -n "polyTweakUV28";
	rename -uid "2987EBAC-4053-EEC7-9F73-E0BE8402826D";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[193]" -type "float2" -0.011794435 -0.018095111 ;
	setAttr ".uvtk[194]" -type "float2" -0.013699436 0.0054797931 ;
createNode polyMergeVert -n "polyMergeVert30";
	rename -uid "101AA1F9-4FB5-39F4-27B1-1F99CC812EA2";
	setAttr ".ics" -type "componentList" 2 "vtx[111]" "vtx[174]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak34";
	rename -uid "295C0D09-4F5F-7CC9-B0C5-9CA0CEA1560C";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[174]" -type "float3" -4.1723251e-007 0.012265921 -0.0052876472 ;
createNode polyBevel3 -n "polyBevel2";
	rename -uid "9247A512-4C8F-E4CB-C5E5-5C8B0172D469";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[343]" "e[351]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".f" 0.2;
	setAttr ".at" 180;
	setAttr ".fn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
	setAttr ".ma" 180;
createNode polyTweakUV -n "polyTweakUV29";
	rename -uid "2842F529-4AF5-A16D-C296-BD871E0DA5D3";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[191]" -type "float2" -0.018301742 -0.024186557 ;
	setAttr ".uvtk[192]" -type "float2" -0.013772091 0.0060788714 ;
createNode polyMergeVert -n "polyMergeVert31";
	rename -uid "7282026F-4A56-AC35-0368-959DCD5F9D19";
	setAttr ".ics" -type "componentList" 2 "vtx[114]" "vtx[183]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak35";
	rename -uid "2FC55184-478C-0F0E-2702-17A5BC88A9EA";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[183]" -type "float3" 0.15820773 0.02365613 -0.39708292 ;
createNode polyTweakUV -n "polyTweakUV30";
	rename -uid "55863000-4F51-B47E-30F8-EFA4F526BDD7";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[184]" -type "float2" -0.011764207 -0.0062807878 ;
	setAttr ".uvtk[199]" -type "float2" -0.017833004 0.025983173 ;
createNode polyMergeVert -n "polyMergeVert32";
	rename -uid "2B2AAD5E-4925-A4DF-F6D1-1C9023623E7E";
	setAttr ".ics" -type "componentList" 2 "vtx[111]" "vtx[176]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak36";
	rename -uid "C6D007FA-424C-FAF0-C826-7EBA6188D7AA";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[176]" -type "float3" 0.13234651 0.024130821 0.40062058 ;
createNode polyTweak -n "polyTweak37";
	rename -uid "F6D4F871-4608-2205-13ED-CB8774D2B4F5";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[173]" -type "float3" 0 0 -0.24436393 ;
	setAttr ".tk[178]" -type "float3" 0 0 0.2443639 ;
createNode polySplit -n "polySplit1";
	rename -uid "C9B45C9E-41FE-5338-7B88-8D9558CEE36C";
	setAttr -s 6 ".e[0:5]"  1 0.54569 0.407763 0.40301701 0.40632501
		 1;
	setAttr -s 6 ".d[0:5]"  -2147483312 -2147483311 -2147483339 -2147483341 -2147483343 -2147483312;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit2";
	rename -uid "C1CB3F33-42A1-301C-BEC4-3CA6855D9D7E";
	setAttr -s 6 ".e[0:5]"  0 0.40531 0.40056801 0.40428901 0.40835199
		 0;
	setAttr -s 6 ".d[0:5]"  -2147483299 -2147483332 -2147483334 -2147483336 -2147483313 -2147483299;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyBevel3 -n "polyBevel3";
	rename -uid "E570350E-4B2F-904D-706B-51A65DCFE92E";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 22 "e[0:1]" "e[3]" "e[48:49]" "e[51]" "e[122]" "e[126:127]" "e[131]" "e[196:197]" "e[206]" "e[208]" "e[211]" "e[219]" "e[221:222]" "e[230:231]" "e[309]" "e[312]" "e[335]" "e[337]" "e[341:343]" "e[345]" "e[370:371]" "e[381:382]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".f" 1;
	setAttr ".at" 180;
	setAttr ".fn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
	setAttr ".ma" 180;
createNode polyTweak -n "polyTweak38";
	rename -uid "F7BA50E8-4597-7BE8-8C2F-0C936956B11B";
	setAttr ".uopa" yes;
	setAttr -s 16 ".tk";
	setAttr ".tk[166]" -type "float3" 0.069292814 0 0 ;
	setAttr ".tk[167]" -type "float3" 0.069292814 0 0 ;
	setAttr ".tk[168]" -type "float3" 0.069292814 0 0 ;
	setAttr ".tk[169]" -type "float3" 0.069292814 0 0 ;
	setAttr ".tk[170]" -type "float3" 0.069292814 0 0 ;
	setAttr ".tk[171]" -type "float3" 0.069292814 0 0 ;
	setAttr ".tk[182]" -type "float3" 0.069292814 0 0 ;
	setAttr ".tk[189]" -type "float3" 0.069292814 0 0 ;
createNode polyTweakUV -n "polyTweakUV31";
	rename -uid "6A38BA28-4664-E0E2-DF9E-E1B98327B226";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[125]" -type "float2" -0.00052486383 -0.0032551216 ;
	setAttr ".uvtk[223]" -type "float2" 0.00068206858 -0.0010988668 ;
createNode polyMergeVert -n "polyMergeVert33";
	rename -uid "EB271C1E-440A-02FC-8387-6494EB51C678";
	setAttr ".ics" -type "componentList" 2 "vtx[149]" "vtx[151]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak39";
	rename -uid "DB79E2AC-42C0-8EEE-B047-6982E351A195";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[149]" -type "float3" 0.0063937306 -0.028280258 0.0016266108 ;
createNode polyTweakUV -n "polyTweakUV32";
	rename -uid "7F8A8679-47AA-00E4-B317-0EA0855F3343";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[127]" -type "float2" -0.0027756863 0.0041024969 ;
	setAttr ".uvtk[235]" -type "float2" 0.00032297373 -0.0011937054 ;
createNode polyMergeVert -n "polyMergeVert34";
	rename -uid "779D463C-4625-C634-5AD7-CCBF744C5713";
	setAttr ".ics" -type "componentList" 1 "vtx[151:152]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak40";
	rename -uid "91CA19C4-40F7-308B-E4D5-6A8444910661";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[151:152]" -type "float3"  0.0050765872 -0.02841711 0.0019958019
		 0 0 0;
createNode polyTweakUV -n "polyTweakUV33";
	rename -uid "F5462A48-42A6-99D2-4E5B-27907792E4BA";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[126]" -type "float2" -0.00057324738 0.0033510725 ;
	setAttr ".uvtk[233]" -type "float2" 0.0007597013 0.0010683665 ;
createNode polyMergeVert -n "polyMergeVert35";
	rename -uid "DDCE104A-436F-001B-795C-1E94A802EF90";
	setAttr ".ics" -type "componentList" 2 "vtx[144]" "vtx[146]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak41";
	rename -uid "3C603B83-4618-8035-1A43-EA98D5FD9DEF";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[146]" -type "float3" 0.004193157 -0.029109955 -0.001365602 ;
createNode polyTweakUV -n "polyTweakUV34";
	rename -uid "EC75B6C2-4055-605B-3027-C794EC7BD3A5";
	setAttr ".uopa" yes;
	setAttr -s 3 ".uvtk";
	setAttr ".uvtk[124]" -type "float2" -0.0023072541 -0.0036622228 ;
	setAttr ".uvtk[220]" -type "float2" 0.0004809956 0.0013972231 ;
createNode polyMergeVert -n "polyMergeVert36";
	rename -uid "9C3BDA24-42E0-46AD-D783-5D8C0397C321";
	setAttr ".ics" -type "componentList" 1 "vtx[146:147]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".d" 1e-006;
createNode polyTweak -n "polyTweak42";
	rename -uid "B1DCD8AF-44EA-4AD5-4B67-40BCC439702C";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk[146:147]" -type "float3"  0.0027738661 -0.028773308
		 -0.0022046566 0 0 0;
createNode polySoftEdge -n "polySoftEdge3";
	rename -uid "F4C6BD5E-49F8-EC37-4BDC-B9BEF6ACEE68";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[0:441]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".a" 180;
createNode polyTweak -n "polyTweak43";
	rename -uid "4B589E25-4648-020D-DEB7-A28A06174561";
	setAttr ".uopa" yes;
	setAttr -s 21 ".tk";
	setAttr ".tk[144]" -type "float3" 0 0.0517825 0 ;
	setAttr ".tk[145]" -type "float3" 0 0.0517825 0 ;
	setAttr ".tk[146]" -type "float3" 0 0.05327348 0 ;
	setAttr ".tk[147]" -type "float3" 0 0.0517825 0 ;
	setAttr ".tk[148]" -type "float3" 0 0.0517825 0 ;
	setAttr ".tk[149]" -type "float3" 0 0.05327348 0 ;
	setAttr ".tk[154]" -type "float3" 0.066162273 -0.031290196 -0.015496258 ;
	setAttr ".tk[157]" -type "float3" 0.015403385 -0.024214583 -0.030424172 ;
	setAttr ".tk[159]" -type "float3" -0.00074743753 -0.024346394 -0.034982901 ;
	setAttr ".tk[160]" -type "float3" -0.014656358 -0.024331143 -0.030924138 ;
	setAttr ".tk[163]" -type "float3" -0.066152602 -0.031288639 -0.015507345 ;
	setAttr ".tk[165]" -type "float3" 0.066252582 -0.031308345 0.015373611 ;
	setAttr ".tk[167]" -type "float3" 0.015456125 -0.024260551 0.030381646 ;
	setAttr ".tk[168]" -type "float3" -0.00075897155 -0.024380906 0.034982901 ;
	setAttr ".tk[171]" -type "float3" -0.014639599 -0.024361622 0.030932276 ;
	setAttr ".tk[172]" -type "float3" -0.066243567 -0.031306803 0.015383682 ;
	setAttr ".tk[174]" -type "float3" 0.075922459 -0.031313002 -7.2842326e-005 ;
	setAttr ".tk[176]" -type "float3" -0.075922459 -0.031311266 -7.3096286e-005 ;
createNode polySmoothFace -n "polySmoothFace1";
	rename -uid "CC2BF09C-40F3-92D7-A14D-D7B8EFBF0ED0";
	setAttr ".ics" -type "componentList" 1 "f[*]";
	setAttr ".sdt" 2;
	setAttr ".dv" 2;
	setAttr ".suv" yes;
	setAttr ".kb" no;
	setAttr ".ma" yes;
	setAttr ".m08" yes;
createNode deleteComponent -n "deleteComponent1";
	rename -uid "D7B42537-4A6E-DBD3-3C67-20AF6DD324F6";
	setAttr ".dc" -type "componentList" 48 "e[1773]" "e[1775]" "e[1779]" "e[1781]" "e[1797]" "e[1799]" "e[1803]" "e[1805]" "e[1821]" "e[1823]" "e[1827]" "e[1829]" "e[1845]" "e[1847]" "e[1851]" "e[1853]" "e[2565]" "e[2567]" "e[2571]" "e[2573]" "e[2589]" "e[2591]" "e[2595]" "e[2597]" "e[2613]" "e[2615]" "e[2619]" "e[2621]" "e[2637]" "e[2639]" "e[2643]" "e[2645]" "e[2757]" "e[2759]" "e[2763]" "e[2765]" "e[2781]" "e[2783]" "e[2787]" "e[2789]" "e[2901]" "e[2903]" "e[2907]" "e[2909]" "e[2925]" "e[2927]" "e[2931]" "e[2933]";
createNode deleteComponent -n "deleteComponent2";
	rename -uid "A7C66041-4CE1-B2C5-7438-D6894B0E2593";
	setAttr ".dc" -type "componentList" 48 "e[1853]" "e[1855]" "e[1859]" "e[1861]" "e[1877]" "e[1879]" "e[1883]" "e[1885]" "e[1901]" "e[1903]" "e[1907]" "e[1909]" "e[1925]" "e[1927]" "e[1931]" "e[1933]" "e[2453]" "e[2455]" "e[2459]" "e[2461]" "e[2477]" "e[2479]" "e[2483]" "e[2485]" "e[2501]" "e[2503]" "e[2507]" "e[2509]" "e[2525]" "e[2527]" "e[2531]" "e[2533]" "e[2765]" "e[2767]" "e[2771]" "e[2773]" "e[2789]" "e[2791]" "e[2795]" "e[2797]" "e[2901]" "e[2903]" "e[2907]" "e[2909]" "e[2925]" "e[2927]" "e[2931]" "e[2933]";
createNode deleteComponent -n "deleteComponent3";
	rename -uid "628134FB-46D2-3D29-5AB9-30A9B54D6BC9";
	setAttr ".dc" -type "componentList" 4 "e[32:47]" "e[128:143]" "e[420:427]" "e[456:463]";
createNode deleteComponent -n "deleteComponent4";
	rename -uid "539B93C2-42BA-B5F5-A6C8-668FE5448B82";
	setAttr ".dc" -type "componentList" 48 "e[1885]" "e[1887]" "e[1891]" "e[1893]" "e[1909]" "e[1911]" "e[1915]" "e[1917]" "e[1933]" "e[1935]" "e[1939]" "e[1941]" "e[1957]" "e[1959]" "e[1963]" "e[1965]" "e[2293]" "e[2295]" "e[2299]" "e[2301]" "e[2317]" "e[2319]" "e[2323]" "e[2325]" "e[2341]" "e[2343]" "e[2347]" "e[2349]" "e[2365]" "e[2367]" "e[2371]" "e[2373]" "e[2725]" "e[2727]" "e[2731]" "e[2733]" "e[2749]" "e[2751]" "e[2755]" "e[2757]" "e[2853]" "e[2855]" "e[2859]" "e[2861]" "e[2877]" "e[2879]" "e[2883]" "e[2885]";
createNode deleteComponent -n "deleteComponent5";
	rename -uid "E3D4ECF4-4801-3F1A-AB90-0F964FF53A44";
	setAttr ".dc" -type "componentList" 35 "vtx[668:671]" "vtx[686:689]" "vtx[706:707]" "vtx[712:713]" "vtx[970]" "vtx[972]" "vtx[979]" "vtx[981]" "vtx[987]" "vtx[989]" "vtx[995]" "vtx[997]" "vtx[1118]" "vtx[1120]" "vtx[1127]" "vtx[1129]" "vtx[1136]" "vtx[1139]" "vtx[1145]" "vtx[1147]" "vtx[1306:1307]" "vtx[1311:1312]" "vtx[1351]" "vtx[1353]" "vtx[1357:1358]" "vtx[2707:2708]" "vtx[2713]" "vtx[2717]" "vtx[2721]" "vtx[2786:2787]" "vtx[2792]" "vtx[2797]" "vtx[2801]" "vtx[2893]" "vtx[2914]";
createNode deleteComponent -n "deleteComponent6";
	rename -uid "2A7F280A-4DA0-B1E1-3DB7-969161F99E76";
	setAttr ".dc" -type "componentList" 40 "vtx[660:663]" "vtx[686:689]" "vtx[694:695]" "vtx[698:699]" "vtx[875]" "vtx[878]" "vtx[886]" "vtx[889]" "vtx[896]" "vtx[899]" "vtx[906]" "vtx[909]" "vtx[1161]" "vtx[1164]" "vtx[1172]" "vtx[1175]" "vtx[1182]" "vtx[1185]" "vtx[1192]" "vtx[1195]" "vtx[1243]" "vtx[1245]" "vtx[1252]" "vtx[1254]" "vtx[1284]" "vtx[1287]" "vtx[1294]" "vtx[1296]" "vtx[2619]" "vtx[2621]" "vtx[2628]" "vtx[2634]" "vtx[2640]" "vtx[2779]" "vtx[2781]" "vtx[2788]" "vtx[2794]" "vtx[2800]" "vtx[2831]" "vtx[2851]";
createNode deleteComponent -n "deleteComponent7";
	rename -uid "D2F3138E-4492-D2C2-17B5-FE8C5B6E2A15";
	setAttr ".dc" -type "componentList" 38 "vtx[660:663]" "vtx[678:681]" "vtx[686:689]" "vtx[897]" "vtx[900]" "vtx[908]" "vtx[911]" "vtx[918]" "vtx[921]" "vtx[928]" "vtx[931]" "vtx[1106]" "vtx[1108]" "vtx[1115]" "vtx[1117]" "vtx[1123]" "vtx[1125]" "vtx[1131]" "vtx[1133]" "vtx[1229]" "vtx[1231]" "vtx[1238]" "vtx[1240]" "vtx[1266]" "vtx[1269]" "vtx[1276]" "vtx[1278]" "vtx[2605]" "vtx[2607]" "vtx[2614]" "vtx[2620]" "vtx[2626]" "vtx[2718:2719]" "vtx[2724]" "vtx[2728]" "vtx[2732]" "vtx[2793]" "vtx[2812]";
createNode deleteComponent -n "deleteComponent8";
	rename -uid "2574CFA4-4ECA-8FE2-4FAA-80B7B87FD4E5";
	setAttr ".dc" -type "componentList" 31 "vtx[10:14]" "vtx[36:40]" "vtx[62]" "vtx[65]" "vtx[889]" "vtx[891]" "vtx[898:899]" "vtx[906:907]" "vtx[914:915]" "vtx[1061]" "vtx[1063]" "vtx[1068:1069]" "vtx[1076:1077]" "vtx[1082:1083]" "vtx[1203]" "vtx[1205]" "vtx[1210:1211]" "vtx[1238:1239]" "vtx[1244:1245]" "vtx[2571]" "vtx[2577]" "vtx[2582]" "vtx[2587]" "vtx[2664]" "vtx[2668]" "vtx[2672]" "vtx[2675]" "vtx[2747]" "vtx[2751]" "vtx[2766]" "vtx[2769]";
createNode deleteComponent -n "deleteComponent9";
	rename -uid "C4B89D1F-47E3-1AE1-F996-2E9C0F5F5B9C";
	setAttr ".dc" -type "componentList" 236 "e[1925]" "e[1927]" "e[1933:1934]" "e[1939]" "e[1941]" "e[1945:1946]" "e[1949]" "e[1951]" "e[1957:1958]" "e[1963]" "e[1965]" "e[1969:1970]" "e[1973]" "e[1975]" "e[1981:1982]" "e[1987]" "e[1989]" "e[1993:1994]" "e[1997]" "e[1999]" "e[2005:2006]" "e[2011]" "e[2013]" "e[2017:2018]" "e[2021]" "e[2023]" "e[2029:2030]" "e[2035]" "e[2037]" "e[2041:2042]" "e[2045]" "e[2047]" "e[2053:2054]" "e[2059]" "e[2061]" "e[2065:2066]" "e[2069]" "e[2071]" "e[2077:2078]" "e[2083]" "e[2085]" "e[2089:2090]" "e[2093]" "e[2095]" "e[2101:2102]" "e[2107]" "e[2109]" "e[2113:2114]" "e[2369]" "e[2371]" "e[2377:2378]" "e[2383]" "e[2385]" "e[2389:2390]" "e[2393]" "e[2395]" "e[2401:2402]" "e[2407]" "e[2409]" "e[2413:2414]" "e[2417]" "e[2419]" "e[2425:2426]" "e[2431]" "e[2433]" "e[2437:2438]" "e[2441]" "e[2443]" "e[2449:2450]" "e[2455]" "e[2457]" "e[2461:2462]" "e[2464]" "e[2468]" "e[2471]" "e[2475]" "e[2477]" "e[2480:2481]" "e[2485]" "e[2488]" "e[2492]" "e[2494]" "e[2497:2498]" "e[2502]" "e[2505]" "e[2509]" "e[2512]" "e[2516]" "e[2519]" "e[2523]" "e[2526]" "e[2530]" "e[2533]" "e[2537]" "e[2539]" "e[2542:2543]" "e[2547]" "e[2550]" "e[2554]" "e[2556]" "e[2559:2560]" "e[2564]" "e[2567]" "e[2571]" "e[2573]" "e[2576:2577]" "e[2581]" "e[2584]" "e[2588]" "e[2590]" "e[2593:2594]" "e[2598]" "e[2601]" "e[2605]" "e[2608]" "e[2612]" "e[2615]" "e[2619]" "e[2622]" "e[2626]" "e[2629]" "e[2633]" "e[2635]" "e[2638:2639]" "e[2643]" "e[2646]" "e[2650]" "e[2652]" "e[2655]" "e[2728]" "e[2734]" "e[2736]" "e[2740]" "e[2742]" "e[2746]" "e[2748]" "e[2751:2752]" "e[2758]" "e[2760]" "e[2764]" "e[2766]" "e[2770]" "e[2772]" "e[2775]" "e[2872]" "e[2878]" "e[2880]" "e[2884]" "e[2886]" "e[2890]" "e[2892]" "e[2895:2896]" "e[2902]" "e[2904]" "e[2908]" "e[2910]" "e[2914]" "e[2916]" "e[2919]" "e[4613]" "e[4615]" "e[4621:4622]" "e[4627]" "e[4629]" "e[4633:4634]" "e[4757]" "e[4759]" "e[4765:4766]" "e[4771]" "e[4773]" "e[4777:4778]" "e[4781]" "e[4783]" "e[4789:4790]" "e[4795]" "e[4797]" "e[4801:4802]" "e[4925]" "e[4927]" "e[4933:4934]" "e[4939]" "e[4941]" "e[4945:4946]" "e[5657]" "e[5659]" "e[5665:5666]" "e[5671]" "e[5673]" "e[5677:5678]" "e[5681]" "e[5683]" "e[5689:5690]" "e[5695]" "e[5697]" "e[5701:5702]" "e[5705]" "e[5707]" "e[5713:5714]" "e[5719]" "e[5721]" "e[5725:5726]" "e[5729]" "e[5731]" "e[5737:5738]" "e[5743]" "e[5745]" "e[5749:5750]" "e[5836]" "e[5842]" "e[5844]" "e[5848]" "e[5850]" "e[5854]" "e[5856]" "e[5859:5860]" "e[5866]" "e[5868]" "e[5872]" "e[5874]" "e[5878]" "e[5880]" "e[5883:5884]" "e[5890]" "e[5892]" "e[5896]" "e[5898]" "e[5902]" "e[5904]" "e[5907:5908]" "e[5914]" "e[5916]" "e[5920]" "e[5922]" "e[5926]" "e[5928]" "e[5931]";
createNode deleteComponent -n "deleteComponent10";
	rename -uid "89ED991E-4F1F-C98B-A633-C285D83206A2";
	setAttr ".dc" -type "componentList" 30 "vtx[288]" "vtx[291:292]" "vtx[294]" "vtx[296]" "vtx[299:300]" "vtx[302]" "vtx[304:306]" "vtx[311:314]" "vtx[319]" "vtx[626:627]" "vtx[980]" "vtx[1008]" "vtx[1156]" "vtx[1163]" "vtx[1170]" "vtx[1188]" "vtx[1192]" "vtx[1202]" "vtx[1251]" "vtx[1288]" "vtx[1941]" "vtx[2000]" "vtx[2312]" "vtx[2323]" "vtx[2719]" "vtx[2721]" "vtx[2740]" "vtx[2742]" "vtx[2971]" "vtx[2973]";
createNode deleteComponent -n "deleteComponent11";
	rename -uid "875FD4B1-4655-237C-7CCD-508AB1CA3474";
	setAttr ".dc" -type "componentList" 22 "vtx[608]" "vtx[926]" "vtx[956]" "vtx[1142]" "vtx[1147]" "vtx[1153]" "vtx[1266]" "vtx[1835]" "vtx[1894]" "vtx[2286]" "vtx[2296]" "vtx[2533]" "vtx[2535]" "vtx[2537]" "vtx[2540]" "vtx[2607:2608]" "vtx[2610:2611]" "vtx[2617]" "vtx[2626]" "vtx[2646]" "vtx[2648]" "vtx[2921]";
createNode deleteComponent -n "deleteComponent12";
	rename -uid "0F753D9B-4E48-6927-9407-11A5DA7083A0";
	setAttr ".dc" -type "componentList" 15 "vtx[206]" "vtx[247:250]" "vtx[988]" "vtx[1135]" "vtx[1137]" "vtx[1145]" "vtx[1255]" "vtx[1959]" "vtx[2273]" "vtx[2721]" "vtx[2724]" "vtx[2789]" "vtx[2791]" "vtx[2832]" "vtx[3022]";
createNode deleteComponent -n "deleteComponent13";
	rename -uid "5D3AF571-40EF-D962-B423-12B1B5EA0E75";
	setAttr ".dc" -type "componentList" 21 "vtx[283:298]" "vtx[599]" "vtx[603]" "vtx[927]" "vtx[951]" "vtx[1132]" "vtx[1134]" "vtx[1140]" "vtx[1154]" "vtx[1159]" "vtx[1165]" "vtx[1203]" "vtx[1250]" "vtx[1820]" "vtx[1883]" "vtx[2265]" "vtx[2273]" "vtx[2644]" "vtx[2669]" "vtx[2883]" "vtx[2886]";
createNode deleteComponent -n "deleteComponent14";
	rename -uid "AA697232-4A74-5A6C-1767-51B4ACEE38B9";
	setAttr ".dc" -type "componentList" 22 "vtx[902]" "vtx[904]" "vtx[910]" "vtx[912]" "vtx[919]" "vtx[922]" "vtx[928]" "vtx[938:939]" "vtx[947:948]" "vtx[955:956]" "vtx[962]" "vtx[2588]" "vtx[2591]" "vtx[2599]" "vtx[2601]" "vtx[2606:2607]" "vtx[2639]" "vtx[2645]" "vtx[2655]" "vtx[2657]" "vtx[2665]" "vtx[2667]";
createNode deleteComponent -n "deleteComponent15";
	rename -uid "CC4D0E67-4D83-C4BE-6E5D-E7A14AEEA30D";
	setAttr ".dc" -type "componentList" 19 "vtx[904:905]" "vtx[910:911]" "vtx[918:919]" "vtx[924]" "vtx[927]" "vtx[929]" "vtx[934]" "vtx[936]" "vtx[940]" "vtx[942]" "vtx[946]" "vtx[2590]" "vtx[2594]" "vtx[2597:2598]" "vtx[2600:2602]" "vtx[2604]" "vtx[2606]" "vtx[2608]" "vtx[2610:2611]";
createNode deleteComponent -n "deleteComponent16";
	rename -uid "D74B2055-492B-048C-D7F6-8685EF2BACD1";
	setAttr ".dc" -type "componentList" 4 "vtx[923]" "vtx[2576]" "vtx[2594]" "vtx[2596]";
createNode deleteComponent -n "deleteComponent17";
	rename -uid "9DE6F813-4A40-1A9A-8441-7790EAFE97F9";
	setAttr ".dc" -type "componentList" 30 "vtx[208:209]" "vtx[215]" "vtx[247:248]" "vtx[252]" "vtx[257]" "vtx[264]" "vtx[267]" "vtx[1039:1040]" "vtx[1043]" "vtx[1051]" "vtx[1054]" "vtx[1096]" "vtx[1101]" "vtx[1106]" "vtx[1153]" "vtx[1827:1828]" "vtx[1831]" "vtx[1887:1888]" "vtx[2155]" "vtx[2161]" "vtx[2208]" "vtx[2656]" "vtx[2666]" "vtx[2694]" "vtx[2703]" "vtx[2804:2805]" "vtx[2813]" "vtx[2817]" "vtx[2915]" "vtx[2917:2918]";
createNode deleteComponent -n "deleteComponent18";
	rename -uid "C578E2EE-4760-A3A9-2EF6-C79CFAF80A4A";
	setAttr ".dc" -type "componentList" 13 "vtx[247:249]" "vtx[251]" "vtx[257]" "vtx[259]" "vtx[577:578]" "vtx[1033:1034]" "vtx[1036]" "vtx[1042:1043]" "vtx[1812:1813]" "vtx[1868:1869]" "vtx[2134]" "vtx[2139]" "vtx[2768:2769]";
createNode deleteComponent -n "deleteComponent19";
	rename -uid "CC2B3D5A-4C86-0E33-0442-6EA324CA9284";
	setAttr ".dc" -type "componentList" 14 "vtx[248:251]" "vtx[254:255]" "vtx[568:569]" "vtx[1033:1034]" "vtx[1036]" "vtx[1043]" "vtx[1045]" "vtx[1731]" "vtx[1733]" "vtx[1791:1792]" "vtx[2123]" "vtx[2129]" "vtx[2743]" "vtx[2747]";
createNode deleteComponent -n "deleteComponent20";
	rename -uid "44DD6889-45F1-75F0-7317-AF88DF5BB499";
	setAttr ".dc" -type "componentList" 15 "vtx[250:253]" "vtx[258:259]" "vtx[556:557]" "vtx[561]" "vtx[1027:1028]" "vtx[1030]" "vtx[1034:1035]" "vtx[1713:1714]" "vtx[1772]" "vtx[1774]" "vtx[2108]" "vtx[2113]" "vtx[2716]" "vtx[2718]" "vtx[2721]";
createNode deleteComponent -n "deleteComponent21";
	rename -uid "40E4A1C0-4448-5DB6-8907-199BF42898CE";
	setAttr ".dc" -type "componentList" 1 "vtx[2502]";
createNode deleteComponent -n "deleteComponent22";
	rename -uid "391E5D19-418D-D296-5C6E-828215324439";
	setAttr ".dc" -type "componentList" 10 "vtx[1047]" "vtx[1049]" "vtx[1055]" "vtx[1091]" "vtx[2310:2311]" "vtx[2321]" "vtx[2332]" "vtx[2374:2375]" "vtx[2388]" "vtx[2402]";
createNode deleteComponent -n "deleteComponent23";
	rename -uid "73C7AC7E-4939-AF4B-2B87-42B7D718189C";
	setAttr ".dc" -type "componentList" 168 "e[2551]" "e[2553]" "e[2557:2558]" "e[2575]" "e[2577]" "e[2581:2582]" "e[2599]" "e[2601]" "e[2605:2606]" "e[2623]" "e[2625]" "e[2629:2630]" "e[2647]" "e[2649]" "e[2653:2654]" "e[2671]" "e[2673]" "e[2677:2678]" "e[2695]" "e[2697]" "e[2701:2702]" "e[2719]" "e[2721]" "e[2725:2726]" "e[3209]" "e[3211]" "e[3217:3218]" "e[3223]" "e[3225]" "e[3229:3230]" "e[3233]" "e[3235]" "e[3241:3242]" "e[3247]" "e[3249]" "e[3253:3254]" "e[3257]" "e[3259]" "e[3265:3266]" "e[3271]" "e[3273]" "e[3277:3278]" "e[3281]" "e[3283]" "e[3289:3290]" "e[3295]" "e[3297]" "e[3301:3302]" "e[3305]" "e[3307]" "e[3313:3314]" "e[3319]" "e[3321]" "e[3325:3326]" "e[3329]" "e[3331]" "e[3337:3338]" "e[3343]" "e[3345]" "e[3349:3350]" "e[3353]" "e[3355]" "e[3361:3362]" "e[3367]" "e[3369]" "e[3373:3374]" "e[3377]" "e[3379]" "e[3385:3386]" "e[3391]" "e[3393]" "e[3397:3398]" "e[3401]" "e[3403]" "e[3409:3410]" "e[3415]" "e[3417]" "e[3421:3422]" "e[3425]" "e[3427]" "e[3433:3434]" "e[3439]" "e[3441]" "e[3445:3446]" "e[3449]" "e[3451]" "e[3457:3458]" "e[3463]" "e[3465]" "e[3469:3470]" "e[3473]" "e[3475]" "e[3481:3482]" "e[3487]" "e[3489]" "e[3493:3494]" "e[3497]" "e[3499]" "e[3505:3506]" "e[3511]" "e[3513]" "e[3517:3518]" "e[3521]" "e[3523]" "e[3529:3530]" "e[3535]" "e[3537]" "e[3541:3542]" "e[3545]" "e[3547]" "e[3553:3554]" "e[3559]" "e[3561]" "e[3565:3566]" "e[3569]" "e[3571]" "e[3577:3578]" "e[3583]" "e[3585]" "e[3589:3590]" "e[3593]" "e[3595]" "e[3601:3602]" "e[3607]" "e[3609]" "e[3613:3614]" "e[3617]" "e[3619]" "e[3625:3626]" "e[3631]" "e[3633]" "e[3637:3638]" "e[3641]" "e[3643]" "e[3649:3650]" "e[3655]" "e[3657]" "e[3661:3662]" "e[3665]" "e[3667]" "e[3673:3674]" "e[3679]" "e[3681]" "e[3685:3686]" "e[3689]" "e[3691]" "e[3697:3698]" "e[3703]" "e[3705]" "e[3709:3710]" "e[3713]" "e[3715]" "e[3721:3722]" "e[3727]" "e[3729]" "e[3733:3734]" "e[3737]" "e[3739]" "e[3745:3746]" "e[3751]" "e[3753]" "e[3757:3758]" "e[3761]" "e[3763]" "e[3769:3770]" "e[3775]" "e[3777]" "e[3781:3782]";
createNode deleteComponent -n "deleteComponent24";
	rename -uid "FCADB7EB-4334-9EA9-0824-BC95E25C1F57";
	setAttr ".dc" -type "componentList" 29 "vtx[284:287]" "vtx[290:291]" "vtx[294:295]" "vtx[478:509]" "vtx[1429:1430]" "vtx[1433]" "vtx[1435:1436]" "vtx[1438]" "vtx[1441:1442]" "vtx[1446]" "vtx[1448]" "vtx[1451:1452]" "vtx[1456]" "vtx[1458]" "vtx[1461]" "vtx[1465]" "vtx[1468:1469]" "vtx[1471]" "vtx[1476]" "vtx[1478]" "vtx[1482]" "vtx[1484]" "vtx[1488]" "vtx[1540:1541]" "vtx[1543]" "vtx[1545:1549]" "vtx[1552:1555]" "vtx[1558:1560]" "vtx[1563]";
createNode deleteComponent -n "deleteComponent25";
	rename -uid "71A4F981-46A9-DF94-61C1-DB85827F7E5E";
	setAttr ".dc" -type "componentList" 6 "vtx[280:287]" "vtx[1105:1106]" "vtx[1108]" "vtx[1114:1115]" "vtx[1122:1123]" "vtx[1129]";
createNode deleteComponent -n "deleteComponent26";
	rename -uid "5C46FC2F-42F6-BBA0-5EB8-A1A411B81B8A";
	setAttr ".dc" -type "componentList" 6 "vtx[257:259]" "vtx[263:267]" "vtx[1398:1400]" "vtx[1403:1404]" "vtx[1407:1408]" "vtx[1411]";
createNode deleteComponent -n "deleteComponent27";
	rename -uid "37AA1ACC-4FB1-4AED-E224-3FA7BF439139";
	setAttr ".dc" -type "componentList" 9 "vtx[272:273]" "vtx[275]" "vtx[280:281]" "vtx[284:285]" "vtx[1113:1114]" "vtx[1116]" "vtx[1122:1123]" "vtx[1130:1131]" "vtx[1137]";
createNode deleteComponent -n "deleteComponent28";
	rename -uid "B44530DF-4349-BF2C-F5C7-72B269D79358";
	setAttr ".dc" -type "componentList" 27 "vtx[274:275]" "vtx[415:446]" "vtx[1310:1311]" "vtx[1314]" "vtx[1316:1317]" "vtx[1319]" "vtx[1322:1323]" "vtx[1327]" "vtx[1329]" "vtx[1332:1333]" "vtx[1337]" "vtx[1339]" "vtx[1342]" "vtx[1346]" "vtx[1381:1382]" "vtx[1384]" "vtx[1389]" "vtx[1391]" "vtx[1395]" "vtx[1397]" "vtx[1401]" "vtx[1405]" "vtx[1410:1412]" "vtx[1414]" "vtx[1417:1418]" "vtx[1423:1424]" "vtx[1428]";
createNode deleteComponent -n "deleteComponent29";
	rename -uid "E9595B1C-46D1-82CE-34EC-12A7A190ADF6";
	setAttr ".dc" -type "componentList" 6 "vtx[273:278]" "vtx[1347]" "vtx[1349]" "vtx[1351]" "vtx[1354:1355]" "vtx[1358]";
createNode deleteComponent -n "deleteComponent30";
	rename -uid "B5612DA8-47A2-645B-3172-0D8B42A3C347";
	setAttr ".dc" -type "componentList" 6 "vtx[261:263]" "vtx[267:271]" "vtx[1327:1329]" "vtx[1332:1333]" "vtx[1336:1337]" "vtx[1340]";
createNode deleteComponent -n "deleteComponent31";
	rename -uid "5C3779BC-4CC7-3926-45FA-3CB50B598AB5";
	setAttr ".dc" -type "componentList" 524 "e[1544]" "e[1557]" "e[1560]" "e[1578]" "e[1582]" "e[1585]" "e[1589]" "e[1591]" "e[1594]" "e[1599]" "e[1602]" "e[1606]" "e[1612]" "e[1623]" "e[1640]" "e[1644]" "e[1647]" "e[1651]" "e[1658]" "e[1661]" "e[1668]" "e[1681]" "e[1684]" "e[1702]" "e[1706]" "e[1709]" "e[1713]" "e[1715]" "e[1718]" "e[1723]" "e[1726]" "e[1730]" "e[1736]" "e[1742]" "e[1751:1752]" "e[1769]" "e[1771]" "e[1775:1776]" "e[1787]" "e[1789]" "e[1792]" "e[1795]" "e[1877]" "e[1879]" "e[1884]" "e[1889]" "e[1895]" "e[1897]" "e[1903:1904]" "e[1919]" "e[1921]" "e[1926]" "e[1931]" "e[1936]" "e[1947]" "e[1964]" "e[1968]" "e[1971]" "e[1975]" "e[1982]" "e[1985]" "e[1992]" "e[2005]" "e[2008]" "e[2026]" "e[2030]" "e[2033]" "e[2037]" "e[2039]" "e[2042]" "e[2047]" "e[2050]" "e[2054]" "e[2060]" "e[2073]" "e[2076]" "e[2094]" "e[2098]" "e[2101]" "e[2105]" "e[2107]" "e[2110]" "e[2115]" "e[2118]" "e[2122]" "e[2140]" "e[2143]" "e[2145]" "e[2147]" "e[2160]" "e[2163]" "e[2165]" "e[2167]" "e[2248]" "e[2254]" "e[2256]" "e[2260]" "e[2262]" "e[2266]" "e[2268]" "e[2271]" "e[2296]" "e[2302]" "e[2316]" "e[2319]" "e[2340]" "e[2346]" "e[2360]" "e[2363]" "e[2388]" "e[2394]" "e[2396]" "e[2400]" "e[2402]" "e[2406]" "e[2408]" "e[2411]" "e[2420]" "e[2424]" "e[2426]" "e[2430]" "e[2464]" "e[2468]" "e[2470]" "e[2474]" "e[2480]" "e[2486]" "e[2488]" "e[2492]" "e[2494]" "e[2496]" "e[2514]" "e[2520]" "e[2522]" "e[2526]" "e[2528]" "e[2530]" "e[2548]" "e[2554]" "e[2556]" "e[2560]" "e[2562]" "e[2564]" "e[2582]" "e[2588]" "e[2590]" "e[2594]" "e[2596]" "e[2598]" "e[2617]" "e[2623]" "e[2625]" "e[2629]" "e[2631]" "e[2635]" "e[2637]" "e[2640:2641]" "e[2647]" "e[2649]" "e[2653]" "e[2655]" "e[2659]" "e[2661]" "e[2664:2665]" "e[2671]" "e[2673]" "e[2677]" "e[2679]" "e[2683]" "e[2685]" "e[2688:2689]" "e[2695]" "e[2697]" "e[2701]" "e[2703]" "e[2707]" "e[2709]" "e[2712:2713]" "e[2719]" "e[2721]" "e[2725]" "e[2727]" "e[2731]" "e[2733]" "e[2736:2737]" "e[2743]" "e[2745]" "e[2749]" "e[2751]" "e[2755]" "e[2757]" "e[2760:2761]" "e[2767]" "e[2769]" "e[2773]" "e[2775]" "e[2779]" "e[2781]" "e[2784]" "e[2793]" "e[2797]" "e[2799]" "e[2803]" "e[2833]" "e[2839]" "e[2853]" "e[2856]" "e[2881]" "e[2887]" "e[2901]" "e[2904:2905]" "e[2911]" "e[2913]" "e[2917]" "e[2919]" "e[2923]" "e[2925]" "e[2928]" "e[2937]" "e[2941]" "e[2943]" "e[2947]" "e[2953]" "e[2959]" "e[2962]" "e[2964]" "e[2967:2970]" "e[2980]" "e[2982]" "e[2985]" "e[2988]" "e[3007]" "e[3013]" "e[3022:3023]" "e[3043]" "e[3049]" "e[3058:3059]" "e[3061]" "e[3067]" "e[3070]" "e[3072]" "e[3075:3078]" "e[3088]" "e[3090]" "e[3093]" "e[3096]" "e[3109]" "e[3112]" "e[3114]" "e[3116]" "e[3129]" "e[3132]" "e[3134]" "e[3136]" "e[3149]" "e[3152]" "e[3154]" "e[3156]" "e[3169]" "e[3172]" "e[3174]" "e[3176]" "e[3179]" "e[3182]" "e[3184]" "e[3186]" "e[3199]" "e[3202]" "e[3204]" "e[3206]" "e[3219]" "e[3222]" "e[3224]" "e[3226]" "e[3239]" "e[3242]" "e[3244]" "e[3246]" "e[3269]" "e[3272]" "e[3274]" "e[3276]" "e[3289]" "e[3292]" "e[3294]" "e[3296]" "e[3309]" "e[3312]" "e[3314]" "e[3316]" "e[3329]" "e[3332]" "e[3334]" "e[3336]" "e[3361]" "e[3367]" "e[3369]" "e[3373]" "e[3375]" "e[3379]" "e[3381]" "e[3384:3385]" "e[3391]" "e[3393]" "e[3397]" "e[3399]" "e[3403]" "e[3405]" "e[3408]" "e[3433]" "e[3439]" "e[3453]" "e[3456]" "e[3489]" "e[3493]" "e[3495]" "e[3499]" "e[3505]" "e[3511]" "e[3513]" "e[3517]" "e[3519]" "e[3523]" "e[3525]" "e[3528]" "e[3537]" "e[3541]" "e[3543]" "e[3547]" "e[3553]" "e[3559]" "e[3561]" "e[3565]" "e[3567]" "e[3571]" "e[3573]" "e[3576:3577]" "e[3583]" "e[3597]" "e[3600]" "e[3709]" "e[3715]" "e[3717]" "e[3721]" "e[3723]" "e[3727]" "e[3729]" "e[3732:3733]" "e[3739]" "e[3741]" "e[3745]" "e[3747]" "e[3751]" "e[3753]" "e[3756]" "e[3768]" "e[3770]" "e[3776:3777]" "e[3878]" "e[3880]" "e[3884:3885]" "e[3908]" "e[3910]" "e[3916:3917]" "e[4018]" "e[4020]" "e[4024:4025]" "e[4278]" "e[4280]" "e[4286:4287]" "e[4292]" "e[4294]" "e[4298:4299]" "e[4326]" "e[4328]" "e[4334:4335]" "e[4340]" "e[4342]" "e[4346:4347]" "e[4469]" "e[4475]" "e[4489]" "e[4492]" "e[4517]" "e[4523]" "e[4525]" "e[4529]" "e[4531]" "e[4535]" "e[4537]" "e[4540]" "e[4549]" "e[4553]" "e[4555]" "e[4559]" "e[4565]" "e[4571]" "e[4585]" "e[4588]" "e[4613]" "e[4619]" "e[4621]" "e[4625]" "e[4627]" "e[4631]" "e[4633]" "e[4636]" "e[4645]" "e[4649]" "e[4651]" "e[4655]" "e[4661]" "e[4667]" "e[4676:4677]" "e[4694]" "e[4696]" "e[4700:4701]" "e[4703]" "e[4709]" "e[4711]" "e[4715]" "e[4717:4718]" "e[4720:4721]" "e[4723:4726]" "e[4736]" "e[4738]" "e[4741]" "e[4744]" "e[4786]" "e[4788]" "e[4793]" "e[4798]" "e[4804]" "e[4806]" "e[4812:4813]" "e[4827:4828]" "e[4830]" "e[4833]" "e[4835:4837]" "e[4839]" "e[4841]" "e[4845]" "e[4847]" "e[4850]" "e[4852]" "e[4854]" "e[4859]" "e[4864]" "e[4910]" "e[4916]" "e[4918]" "e[4922]" "e[4924]" "e[4928]" "e[4930]" "e[4933]" "e[5006]" "e[5012]" "e[5014]" "e[5018]" "e[5020]" "e[5024]" "e[5026]" "e[5029]" "e[5174]" "e[5180]" "e[5182]" "e[5186]" "e[5188]" "e[5192]" "e[5194]" "e[5197:5198]" "e[5204]" "e[5206]" "e[5210]" "e[5212]" "e[5216]" "e[5218]" "e[5221]" "e[5318]" "e[5324]" "e[5326]" "e[5330]" "e[5332]" "e[5336]" "e[5338]" "e[5341]" "e[5343]" "e[5345]" "e[5351:5352]" "e[5357]" "e[5359]" "e[5363:5364]" "e[5366]" "e[5372]" "e[5374]" "e[5378]" "e[5380]" "e[5384]" "e[5386]" "e[5389:5390]" "e[5396]" "e[5398]" "e[5402]" "e[5404]" "e[5408]" "e[5410]" "e[5413]" "e[5504]" "e[5510]" "e[5512]" "e[5516]" "e[5518]" "e[5522]" "e[5524]" "e[5527]" "e[5624]" "e[5630]" "e[5632]" "e[5636]" "e[5638]" "e[5642]" "e[5644]" "e[5647]";
createNode deleteComponent -n "deleteComponent32";
	rename -uid "553CF1D4-4327-6CE4-4150-6FB60524752D";
	setAttr ".dc" -type "componentList" 42 "vtx[230:232]" "vtx[239]" "vtx[257:258]" "vtx[269]" "vtx[274]" "vtx[297:299]" "vtx[348:349]" "vtx[387:388]" "vtx[394:395]" "vtx[819]" "vtx[845]" "vtx[873]" "vtx[877]" "vtx[993]" "vtx[995]" "vtx[998]" "vtx[1051:1052]" "vtx[1061]" "vtx[1065]" "vtx[1085]" "vtx[1107]" "vtx[1109]" "vtx[1112]" "vtx[1186]" "vtx[1192]" "vtx[1243]" "vtx[1263:1265]" "vtx[1273:1274]" "vtx[1278]" "vtx[1312]" "vtx[1315]" "vtx[1322]" "vtx[1325]" "vtx[1351:1352]" "vtx[1396]" "vtx[1453]" "vtx[2323]" "vtx[2325]" "vtx[2347]" "vtx[2353]" "vtx[2384]" "vtx[2392]";
createNode polyTweak -n "polyTweak44";
	rename -uid "F08EFCC5-40ED-F0A8-5680-4F9604C0CA6B";
	setAttr ".uopa" yes;
	setAttr -s 134 ".tk";
	setAttr ".tk[295]" -type "float3" 0 0.0087414384 0 ;
	setAttr ".tk[305]" -type "float3" 0 0.0087414384 0 ;
	setAttr ".tk[310]" -type "float3" 0 0.0087414384 0 ;
	setAttr ".tk[1097]" -type "float3" 0 0.0087414384 0 ;
	setAttr ".tk[1100]" -type "float3" 0 0.0087414384 0 ;
	setAttr ".tk[1661]" -type "float3" 0 0.0087414384 0 ;
	setAttr ".tk[1662]" -type "float3" 0 0.0087414384 0 ;
	setAttr ".tk[1664]" -type "float3" 0 0.0087414384 0 ;
	setAttr ".tk[1760]" -type "float3" 0 0.0087414384 0 ;
	setAttr ".tk[1873]" -type "float3" 0 -2.9802322e-008 0 ;
	setAttr ".tk[1877]" -type "float3" 0 -2.9802322e-008 0 ;
	setAttr ".tk[1914]" -type "float3" 0 -2.9802322e-008 0 ;
	setAttr ".tk[1916]" -type "float3" 0 -2.9802322e-008 0 ;
	setAttr ".tk[1921]" -type "float3" 0 -2.9802322e-008 0 ;
	setAttr ".tk[1962]" -type "float3" 0 -2.9802322e-008 0 ;
	setAttr ".tk[1963]" -type "float3" 0 -2.9802322e-008 0 ;
	setAttr ".tk[1973]" -type "float3" 0 -2.9802322e-008 0 ;
	setAttr ".tk[1998]" -type "float3" 0 -2.9802322e-008 0 ;
	setAttr ".tk[2499]" -type "float3" 0 -2.9802322e-008 0 ;
	setAttr ".tk[2501]" -type "float3" 0 -2.9802322e-008 0 ;
	setAttr ".tk[2558]" -type "float3" 0 -2.9802322e-008 0 ;
	setAttr ".tk[2561]" -type "float3" 0 -2.9802322e-008 0 ;
	setAttr ".tk[2716]" -type "float3" 0 0.0087414384 0 ;
	setAttr ".tk[2717]" -type "float3" 0 0.0087414384 0 ;
	setAttr ".tk[2719]" -type "float3" 0 0.0087414384 0 ;
	setAttr ".tk[2721]" -type "float3" 0 0.0087414384 0 ;
	setAttr ".tk[2735]" -type "float3" 0 -2.9802322e-008 0 ;
	setAttr ".tk[2746]" -type "float3" 0 -2.9802322e-008 0 ;
	setAttr ".tk[2764]" -type "float3" 0 -2.9802322e-008 0 ;
	setAttr ".tk[2765]" -type "float3" 0 -2.9802322e-008 0 ;
createNode deleteComponent -n "deleteComponent33";
	rename -uid "8A4C21BB-4E76-B3A6-E0D9-5AB9CDA9A817";
	setAttr ".dc" -type "componentList" 40 "vtx[295]" "vtx[305]" "vtx[310]" "vtx[874:876]" "vtx[883:884]" "vtx[1097]" "vtx[1100]" "vtx[1661:1662]" "vtx[1664]" "vtx[1760]" "vtx[1788]" "vtx[1790]" "vtx[1828]" "vtx[1831]" "vtx[1850]" "vtx[1852]" "vtx[1873]" "vtx[1877]" "vtx[1914]" "vtx[1916]" "vtx[1921]" "vtx[1962:1963]" "vtx[1973]" "vtx[1998]" "vtx[2021]" "vtx[2357]" "vtx[2359]" "vtx[2373]" "vtx[2388]" "vtx[2495:2496]" "vtx[2499]" "vtx[2501]" "vtx[2558]" "vtx[2561]" "vtx[2716:2717]" "vtx[2719]" "vtx[2721]" "vtx[2735]" "vtx[2746]" "vtx[2764:2765]";
createNode deleteComponent -n "deleteComponent34";
	rename -uid "C80085A0-40F0-E08E-9509-2494A169969C";
	setAttr ".dc" -type "componentList" 69 "vtx[249:250]" "vtx[256]" "vtx[261]" "vtx[269]" "vtx[272]" "vtx[276]" "vtx[283]" "vtx[298]" "vtx[302]" "vtx[309]" "vtx[314]" "vtx[343:344]" "vtx[389]" "vtx[392]" "vtx[643]" "vtx[647]" "vtx[673]" "vtx[701]" "vtx[918]" "vtx[920]" "vtx[923]" "vtx[995:996]" "vtx[1005]" "vtx[1009]" "vtx[1037]" "vtx[1044]" "vtx[1054]" "vtx[1057]" "vtx[1065]" "vtx[1077]" "vtx[1084]" "vtx[1097]" "vtx[1100]" "vtx[1160:1161]" "vtx[1234:1235]" "vtx[1239]" "vtx[1248:1250]" "vtx[1253]" "vtx[1256]" "vtx[1274]" "vtx[1279]" "vtx[1298]" "vtx[1301]" "vtx[1352]" "vtx[1400]" "vtx[1631:1632]" "vtx[1634]" "vtx[1717]" "vtx[1900:1901]" "vtx[1903]" "vtx[1933]" "vtx[1935]" "vtx[1943]" "vtx[2030:2032]" "vtx[2042]" "vtx[2100:2102]" "vtx[2109]" "vtx[2137]" "vtx[2145]" "vtx[2504]" "vtx[2513]" "vtx[2645]" "vtx[2648]" "vtx[2651]" "vtx[2653]" "vtx[2667]" "vtx[2670]" "vtx[2705]" "vtx[2715]";
createNode deleteComponent -n "deleteComponent35";
	rename -uid "15E4BB8A-4E71-396B-A0B5-0B95705A0724";
	setAttr ".dc" -type "componentList" 117 "vtx[248:249]" "vtx[253]" "vtx[257]" "vtx[263]" "vtx[267:269]" "vtx[275]" "vtx[277]" "vtx[280:281]" "vtx[287]" "vtx[289]" "vtx[292]" "vtx[294]" "vtx[298:299]" "vtx[303]" "vtx[327]" "vtx[330]" "vtx[371]" "vtx[374]" "vtx[377:378]" "vtx[413:414]" "vtx[625]" "vtx[630]" "vtx[653]" "vtx[682]" "vtx[849:851]" "vtx[856:857]" "vtx[894]" "vtx[897]" "vtx[902]" "vtx[970]" "vtx[972]" "vtx[981:982]" "vtx[1011]" "vtx[1014]" "vtx[1024]" "vtx[1027]" "vtx[1029]" "vtx[1032]" "vtx[1034]" "vtx[1037]" "vtx[1042]" "vtx[1046]" "vtx[1049]" "vtx[1053]" "vtx[1058]" "vtx[1061]" "vtx[1066]" "vtx[1119]" "vtx[1122]" "vtx[1196:1198]" "vtx[1205:1208]" "vtx[1211]" "vtx[1228]" "vtx[1231]" "vtx[1244]" "vtx[1247]" "vtx[1252]" "vtx[1254]" "vtx[1301]" "vtx[1342]" "vtx[1350]" "vtx[1574:1575]" "vtx[1577]" "vtx[1591:1592]" "vtx[1594]" "vtx[1660]" "vtx[1689]" "vtx[1715]" "vtx[1717]" "vtx[1754]" "vtx[1758]" "vtx[1775]" "vtx[1777]" "vtx[1797]" "vtx[1799]" "vtx[1837:1838]" "vtx[1840:1841]" "vtx[1843]" "vtx[1845]" "vtx[1873]" "vtx[1875]" "vtx[1878:1879]" "vtx[1881]" "vtx[1884]" "vtx[1908]" "vtx[1929]" "vtx[1960:1961]" "vtx[1963:1964]" "vtx[2026]" "vtx[2028]" "vtx[2031:2032]" "vtx[2060]" "vtx[2063]" "vtx[2243]" "vtx[2246]" "vtx[2348]" "vtx[2350]" "vtx[2355]" "vtx[2366]" "vtx[2418]" "vtx[2420]" "vtx[2459]" "vtx[2470]" "vtx[2473]" "vtx[2475]" "vtx[2493]" "vtx[2495]" "vtx[2510]" "vtx[2512:2513]" "vtx[2516]" "vtx[2530:2531]" "vtx[2537]" "vtx[2549]" "vtx[2557]" "vtx[2563]" "vtx[2565]" "vtx[2568]";
createNode deleteComponent -n "deleteComponent36";
	rename -uid "0DCD9C26-4FB5-4401-6AC0-60A3810B363D";
	setAttr ".dc" -type "componentList" 32 "vtx[224:227]" "vtx[248:249]" "vtx[255]" "vtx[304]" "vtx[307]" "vtx[336:337]" "vtx[746]" "vtx[773]" "vtx[798]" "vtx[803]" "vtx[898]" "vtx[901]" "vtx[906]" "vtx[947]" "vtx[949]" "vtx[958:959]" "vtx[973]" "vtx[1051]" "vtx[1054]" "vtx[1104]" "vtx[1118:1120]" "vtx[1127:1129]" "vtx[1146]" "vtx[1149]" "vtx[1154]" "vtx[1157]" "vtx[1215]" "vtx[2044:2045]" "vtx[2071]" "vtx[2074]" "vtx[2105]" "vtx[2107]";
createNode deleteComponent -n "deleteComponent37";
	rename -uid "C53365BB-47BE-E200-140B-02B3A277A42B";
	setAttr ".dc" -type "componentList" 65 "vtx[206:208]" "vtx[212]" "vtx[218]" "vtx[232:239]" "vtx[289:291]" "vtx[294]" "vtx[315:316]" "vtx[321:324]" "vtx[703:704]" "vtx[706]" "vtx[710]" "vtx[712]" "vtx[718:719]" "vtx[750]" "vtx[765]" "vtx[767]" "vtx[776]" "vtx[791]" "vtx[795]" "vtx[872]" "vtx[893]" "vtx[1019]" "vtx[1025]" "vtx[1032]" "vtx[1034]" "vtx[1069]" "vtx[1071]" "vtx[1075]" "vtx[1077]" "vtx[1080]" "vtx[1084:1085]" "vtx[1160]" "vtx[1175]" "vtx[1306:1307]" "vtx[1309]" "vtx[1353:1354]" "vtx[1356]" "vtx[1554]" "vtx[1576]" "vtx[1582]" "vtx[1586]" "vtx[1591:1592]" "vtx[1595:1596]" "vtx[1600]" "vtx[1964]" "vtx[1966:1967]" "vtx[1970]" "vtx[1977]" "vtx[1979]" "vtx[1982]" "vtx[1984]" "vtx[1991:1992]" "vtx[2027]" "vtx[2042]" "vtx[2046]" "vtx[2051]" "vtx[2059]" "vtx[2077]" "vtx[2146]" "vtx[2148]" "vtx[2150]" "vtx[2152]" "vtx[2234:2235]" "vtx[2238]" "vtx[2240]";
createNode script -n "uiConfigurationScriptNode";
	rename -uid "C2931CD7-4B11-8D69-0838-86BC00B18922";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 511\n                -height 335\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n"
		+ "            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n"
		+ "            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 511\n            -height 335\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n"
		+ "                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n"
		+ "                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n"
		+ "                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 510\n                -height 334\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n"
		+ "            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n"
		+ "            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n"
		+ "            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 510\n            -height 334\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n"
		+ "                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n"
		+ "                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n"
		+ "                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 511\n                -height 334\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n"
		+ "            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n"
		+ "            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n"
		+ "            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 511\n            -height 334\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 1027\n                -height 714\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n"
		+ "            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n"
		+ "            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1027\n            -height 714\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -docTag \"isolOutln_fromSeln\" \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 1\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n"
		+ "                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 1\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n"
		+ "            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 0\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n"
		+ "            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\tif ($useSceneConfig) {\n\t\toutlinerPanel -e -to $panelName;\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n"
		+ "                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n"
		+ "                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n"
		+ "                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n"
		+ "                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n"
		+ "                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n"
		+ "                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n"
		+ "                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n"
		+ "                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n"
		+ "                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n"
		+ "                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n"
		+ "                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n"
		+ "                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n"
		+ "                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n"
		+ "                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n"
		+ "\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 1\n                -settingsChangedCallback \"nodeEdSyncControls\" \n"
		+ "                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n"
		+ "                -additiveGraphingMode 1\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1027\\n    -height 714\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1027\\n    -height 714\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 100 -size 1200 -divisions 1 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "0574F71D-4C08-BAB7-4396-77934915E598";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode deleteComponent -n "deleteComponent38";
	rename -uid "1B10066E-4CC2-05A8-3384-6592A942ACB3";
	setAttr ".dc" -type "componentList" 19 "vtx[271:272]" "vtx[284]" "vtx[295:296]" "vtx[607]" "vtx[626]" "vtx[630]" "vtx[635:636]" "vtx[965]" "vtx[969]" "vtx[997]" "vtx[1013]" "vtx[1015]" "vtx[1143]" "vtx[1832]" "vtx[1838]" "vtx[1861]" "vtx[1863]" "vtx[1866]" "vtx[1868]";
createNode deleteComponent -n "deleteComponent39";
	rename -uid "4073AF55-4E0E-92CD-9F4E-65B2ED74B0C2";
	setAttr ".dc" -type "componentList" 51 "vtx[227:230]" "vtx[266]" "vtx[269]" "vtx[286:287]" "vtx[333:336]" "vtx[550]" "vtx[565]" "vtx[569]" "vtx[571]" "vtx[591]" "vtx[616]" "vtx[633:634]" "vtx[813]" "vtx[860]" "vtx[941]" "vtx[944]" "vtx[990]" "vtx[1118]" "vtx[1179:1180]" "vtx[1182]" "vtx[1226:1227]" "vtx[1229]" "vtx[1453]" "vtx[1475]" "vtx[1536]" "vtx[1538]" "vtx[1541]" "vtx[1543]" "vtx[1547:1548]" "vtx[1551]" "vtx[1708]" "vtx[1710]" "vtx[1716]" "vtx[1727]" "vtx[1747]" "vtx[1749]" "vtx[1752]" "vtx[1757]" "vtx[1760]" "vtx[1763]" "vtx[1772]" "vtx[1785]" "vtx[1807:1809]" "vtx[1813]" "vtx[1837]" "vtx[1840]" "vtx[1852:1853]" "vtx[2019:2020]" "vtx[2022]" "vtx[2025]" "vtx[2345]";
createNode deleteComponent -n "deleteComponent40";
	rename -uid "9F539C20-43DD-0244-2A0D-3EA2B5325EF5";
	setAttr ".dc" -type "componentList" 1 "vtx[1860]";
createNode deleteComponent -n "deleteComponent41";
	rename -uid "01903B06-475B-72FA-B790-F79FE198B220";
	setAttr ".dc" -type "componentList" 1 "vtx[884]";
createNode polySphere -n "polySphere1";
	rename -uid "E11A33BF-44BF-DF90-0419-CE88A25CC13B";
	setAttr ".sa" 10;
	setAttr ".sh" 10;
createNode polySoftEdge -n "polySoftEdge4";
	rename -uid "0DFA86A3-44E0-84C8-6D81-7C88ADB60705";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[0:189]";
	setAttr ".ix" -type "matrix" 0.32521119232928131 0 0 0 0 0.32521119232928131 0 0
		 0 0 0.32521119232928131 0 2.2465702719403322 15.82465912585004 1.1915283868210895 1;
	setAttr ".a" 180;
createNode polySoftEdge -n "polySoftEdge5";
	rename -uid "5B55510A-4931-60EA-0B59-D9A53FEAD1A9";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[*]";
	setAttr ".ix" -type "matrix" 0.32521119232928131 0 0 0 0 0.32521119232928131 0 0
		 0 0 0.32521119232928131 0 2.24657642540702 15.824581927813409 -1.1914032663317666 1;
	setAttr ".a" 180;
createNode polyExtrudeFace -n "polyExtrudeFace8";
	rename -uid "A32BCE48-4E9D-9A06-C3B1-7FB19B215588";
	setAttr ".ics" -type "componentList" 2 "f[336]" "f[344]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 2.7681899 17.469807 1.3985152e-007 ;
	setAttr ".rs" 44268;
	setAttr ".lt" -type "double3" -4.4617087802123478e-015 -5.0653925498522767e-016 
		-0.31291516106202422 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 2.6939459470279354 17.186316135568493 -0.64683218639177475 ;
	setAttr ".cbx" -type "double3" 2.8424337599269789 17.753298420889273 0.64683246609480605 ;
createNode polySplitRing -n "polySplitRing10";
	rename -uid "FFC376E8-4E1E-7B16-A314-1994310FB8AC";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[4546:4547]" "e[4549]" "e[4551]" "e[4554:4555]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".wt" 0.28688746690750122;
	setAttr ".re" 4546;
	setAttr ".sma" 29.999999999999996;
	setAttr ".div" 1;
	setAttr ".p[0]"  0 0 1;
	setAttr ".uem" no;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak45";
	rename -uid "63382825-4B54-A31B-19A5-4CBD140F0B9D";
	setAttr ".uopa" yes;
	setAttr -s 74 ".tk";
	setAttr ".tk[496]" -type "float3" -2.6077032e-008 -3.7252903e-009 3.7252903e-009 ;
	setAttr ".tk[2279]" -type "float3" -0.0011493372 0.01670905 -2.4959489e-008 ;
	setAttr ".tk[2280]" -type "float3" -0.0043759546 -0.01670791 7.3014778e-010 ;
	setAttr ".tk[2281]" -type "float3" 0.0043759546 0.016707996 0.034706339 ;
	setAttr ".tk[2282]" -type "float3" 0.0012579036 -0.01670826 0.035440505 ;
	setAttr ".tk[2283]" -type "float3" 0.0043756901 0.016706064 -0.03470641 ;
	setAttr ".tk[2284]" -type "float3" 0.001257805 -0.016709052 -0.035440505 ;
createNode polyBevel3 -n "polyBevel4";
	rename -uid "A76FB7B1-4AF5-C0D1-FBBF-4BA5C82AE2BE";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[4550]" "e[4552:4553]" "e[4556:4558]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".f" 0.2;
	setAttr ".at" 180;
	setAttr ".fn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
	setAttr ".ma" 180;
createNode polyTweak -n "polyTweak46";
	rename -uid "615E3A94-4CCD-442A-32BE-2CB1ADEB3E8A";
	setAttr ".uopa" yes;
	setAttr -s 11 ".tk";
	setAttr ".tk[2285]" -type "float3" 0.014283278 0 0 ;
	setAttr ".tk[2286]" -type "float3" 0.014283278 0 0 ;
	setAttr ".tk[2287]" -type "float3" 0.014283278 0 0 ;
	setAttr ".tk[2288]" -type "float3" 0.014283278 0 0 ;
	setAttr ".tk[2289]" -type "float3" 0.014283278 0 0 ;
	setAttr ".tk[2290]" -type "float3" 0.014283278 0 0 ;
createNode polySoftEdge -n "polySoftEdge6";
	rename -uid "5B63A110-4E6A-4069-5918-4486D0B7BEBA";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[272:273]" "e[1845:1846]" "e[1852]" "e[1854]" "e[4552:4557]";
	setAttr ".ix" -type "matrix" 6.2568508953739101 0 0 0 0 6.2568508953739101 0 0 0 0 6.2568508953739101 0
		 0 20.765924948942121 0 1;
	setAttr ".a" 180;
createNode polyTweak -n "polyTweak47";
	rename -uid "2DC58F65-4297-D5B5-E7E1-D5B725DFBD69";
	setAttr ".uopa" yes;
	setAttr -s 9 ".tk";
	setAttr ".tk[2286]" -type "float3" 0.0037598629 0 0 ;
	setAttr ".tk[2288]" -type "float3" 0.0037598629 0 0 ;
	setAttr ".tk[2290]" -type "float3" 0.0037598629 0 0 ;
	setAttr ".tk[2292]" -type "float3" 0.0037598629 0 0 ;
	setAttr ".tk[2294]" -type "float3" 0.0037598629 0 0 ;
	setAttr ".tk[2296]" -type "float3" 0.0037598629 0 0 ;
createNode deleteComponent -n "deleteComponent42";
	rename -uid "A7D11D38-40DD-B65B-CF01-21AF47E5225E";
	setAttr ".dc" -type "componentList" 1 "vtx[992]";
createNode deleteComponent -n "deleteComponent43";
	rename -uid "754BAD90-46B3-3810-0120-0E9B8E67606D";
	setAttr ".dc" -type "componentList" 3 "vtx[278]" "vtx[960]" "vtx[965]";
createNode polyDelEdge -n "polyDelEdge1";
	rename -uid "4D9482A8-483C-FBC8-896A-51B446BDDBAD";
	setAttr ".ics" -type "componentList" 163 "e[436:437]" "e[446:451]" "e[460:461]" "e[470:471]" "e[772:779]" "e[792:795]" "e[816:821]" "e[830:835]" "e[992:995]" "e[1004:1007]" "e[1016:1019]" "e[1028:1031]" "e[1036:1039]" "e[1052:1055]" "e[1072:1075]" "e[1088:1091]" "e[1120:1123]" "e[1140:1143]" "e[1152:1155]" "e[1160:1163]" "e[1318:1377]" "e[2220]" "e[2222]" "e[2230]" "e[2232]" "e[2658]" "e[2660]" "e[2664]" "e[2666]" "e[2680]" "e[2682]" "e[2690]" "e[2692]" "e[2702]" "e[2704]" "e[2708]" "e[2710]" "e[2859]" "e[2861]" "e[2873]" "e[2876]" "e[2886]" "e[2888]" "e[2892]" "e[2894]" "e[2944]" "e[2946]" "e[2954]" "e[2956]" "e[3218]" "e[3220]" "e[3224]" "e[3226]" "e[3242]" "e[3244]" "e[3248]" "e[3250]" "e[3266]" "e[3268]" "e[3272]" "e[3274]" "e[3290]" "e[3292]" "e[3296]" "e[3298]" "e[3314]" "e[3316]" "e[3320]" "e[3322]" "e[3338]" "e[3340]" "e[3344]" "e[3346]" "e[3386]" "e[3388]" "e[3392]" "e[3394]" "e[3410]" "e[3412]" "e[3416]" "e[3418]" "e[3526]" "e[3528]" "e[3532]" "e[3534]" "e[3550]" "e[3552]" "e[3556]" "e[3558]" "e[3574]" "e[3576]" "e[3580]" "e[3582]" "e[3598]" "e[3600]" "e[3604]" "e[3606]" "e[4047]" "e[4049]" "e[4053]" "e[4055]" "e[4071]" "e[4073]" "e[4077]" "e[4079]" "e[4143]" "e[4145]" "e[4149]" "e[4151]" "e[4165]" "e[4167]" "e[4175]" "e[4177]" "e[4187]" "e[4189]" "e[4193]" "e[4195]" "e[4211]" "e[4213]" "e[4217]" "e[4219]" "e[4256]" "e[4258]" "e[4270]" "e[4273]" "e[4281]" "e[4283]" "e[4288]" "e[4296]" "e[4323]" "e[4325]" "e[4329]" "e[4331]" "e[4347]" "e[4349]" "e[4353]" "e[4355]" "e[4392]" "e[4394:4395]" "e[4397]" "e[4411]" "e[4413]" "e[4423]" "e[4425]" "e[4429]" "e[4431]" "e[4447]" "e[4449]" "e[4453]" "e[4455]" "e[4471]" "e[4473]" "e[4477]" "e[4479]" "e[4495]" "e[4497]" "e[4501]" "e[4503]" "e[4517]" "e[4519]" "e[4526]" "e[4528:4529]" "e[4531]";
	setAttr ".cv" yes;
createNode polyDelEdge -n "polyDelEdge2";
	rename -uid "3DD87D9E-4DA8-0AC5-CC61-7B8D9459CB46";
	setAttr ".ics" -type "componentList" 60 "e[2026]" "e[2028]" "e[2040]" "e[2043:2044]" "e[2061]" "e[2063]" "e[2067:2068]" "e[2081]" "e[2083]" "e[2086]" "e[2102]" "e[2104]" "e[2108:2109]" "e[2122]" "e[2124]" "e[2127]" "e[2135]" "e[2137]" "e[2149]" "e[2152:2153]" "e[2450]" "e[2454:2455]" "e[2460]" "e[2462]" "e[2465]" "e[2467]" "e[2469]" "e[2475:2476]" "e[2481]" "e[2483]" "e[2487:2488]" "e[2491]" "e[2493]" "e[2498]" "e[2501]" "e[2504:2505]" "e[2508]" "e[2511]" "e[2514]" "e[2516]" "e[2518]" "e[2520]" "e[2525]" "e[2528]" "e[2531:2532]" "e[2535]" "e[2538]" "e[2541]" "e[2543]" "e[2545]" "e[2549:2550]" "e[2555]" "e[2557]" "e[2560]" "e[2562]" "e[2564]" "e[2570:2571]" "e[2576]" "e[2578]" "e[2582:2583]";
	setAttr ".cv" yes;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr -s 3 ".dsm";
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "polyDelEdge2.out" "pCubeShape1.i";
connectAttr "polyTweakUV34.uvtk[0]" "pCubeShape1.uvst[0].uvtw";
connectAttr "polySoftEdge4.out" "pSphereShape1.i";
connectAttr "polySoftEdge5.out" "pSphereShape2.i";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polyTweak1.out" "polySoftEdge1.ip";
connectAttr "pCubeShape1.wm" "polySoftEdge1.mp";
connectAttr "polyCube1.out" "polyTweak1.ip";
connectAttr "polySoftEdge1.out" "polySoftEdge2.ip";
connectAttr "pCubeShape1.wm" "polySoftEdge2.mp";
connectAttr "polySoftEdge2.out" "polySplitRing1.ip";
connectAttr "pCubeShape1.wm" "polySplitRing1.mp";
connectAttr "polySplitRing1.out" "polyExtrudeFace1.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace1.mp";
connectAttr "polyExtrudeFace1.out" "polyExtrudeFace2.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace2.mp";
connectAttr "polyExtrudeFace2.out" "polyExtrudeFace3.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace3.mp";
connectAttr "polyTweak2.out" "polyExtrudeFace4.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace4.mp";
connectAttr "polyExtrudeFace3.out" "polyTweak2.ip";
connectAttr "polyTweak3.out" "polyExtrudeFace5.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace5.mp";
connectAttr "polyExtrudeFace4.out" "polyTweak3.ip";
connectAttr "polyExtrudeFace5.out" "polyExtrudeFace6.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace6.mp";
connectAttr "polyExtrudeFace6.out" "polyMergeVert1.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert1.mp";
connectAttr "polyMergeVert1.out" "polyMergeVert2.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert2.mp";
connectAttr "polyTweak4.out" "polySplitRing2.ip";
connectAttr "pCubeShape1.wm" "polySplitRing2.mp";
connectAttr "polyMergeVert2.out" "polyTweak4.ip";
connectAttr "polySplitRing2.out" "polySplitRing3.ip";
connectAttr "pCubeShape1.wm" "polySplitRing3.mp";
connectAttr "polyTweak5.out" "polyExtrudeFace7.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace7.mp";
connectAttr "polySplitRing3.out" "polyTweak5.ip";
connectAttr "polyTweak6.out" "polySplitRing4.ip";
connectAttr "pCubeShape1.wm" "polySplitRing4.mp";
connectAttr "polyExtrudeFace7.out" "polyTweak6.ip";
connectAttr "polySplitRing4.out" "polySplitRing5.ip";
connectAttr "pCubeShape1.wm" "polySplitRing5.mp";
connectAttr "polySplitRing5.out" "polySplitRing6.ip";
connectAttr "pCubeShape1.wm" "polySplitRing6.mp";
connectAttr "polySplitRing6.out" "polySplitRing7.ip";
connectAttr "pCubeShape1.wm" "polySplitRing7.mp";
connectAttr "polySplitRing7.out" "polySplitRing8.ip";
connectAttr "pCubeShape1.wm" "polySplitRing8.mp";
connectAttr "polySplitRing8.out" "polySplitRing9.ip";
connectAttr "pCubeShape1.wm" "polySplitRing9.mp";
connectAttr "polySplitRing9.out" "polyTweakUV1.ip";
connectAttr "polyTweak7.out" "polyMergeVert3.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert3.mp";
connectAttr "polyTweakUV1.out" "polyTweak7.ip";
connectAttr "polyMergeVert3.out" "polyTweakUV2.ip";
connectAttr "polyTweak8.out" "polyMergeVert4.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert4.mp";
connectAttr "polyTweakUV2.out" "polyTweak8.ip";
connectAttr "polyMergeVert4.out" "polyTweakUV3.ip";
connectAttr "polyTweak9.out" "polyMergeVert5.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert5.mp";
connectAttr "polyTweakUV3.out" "polyTweak9.ip";
connectAttr "polyMergeVert5.out" "polyTweakUV4.ip";
connectAttr "polyTweak10.out" "polyMergeVert6.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert6.mp";
connectAttr "polyTweakUV4.out" "polyTweak10.ip";
connectAttr "polyMergeVert6.out" "polyTweakUV5.ip";
connectAttr "polyTweak11.out" "polyMergeVert7.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert7.mp";
connectAttr "polyTweakUV5.out" "polyTweak11.ip";
connectAttr "polyMergeVert7.out" "polyTweakUV6.ip";
connectAttr "polyTweak12.out" "polyMergeVert8.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert8.mp";
connectAttr "polyTweakUV6.out" "polyTweak12.ip";
connectAttr "polyMergeVert8.out" "polyTweakUV7.ip";
connectAttr "polyTweak13.out" "polyMergeVert9.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert9.mp";
connectAttr "polyTweakUV7.out" "polyTweak13.ip";
connectAttr "polyMergeVert9.out" "polyTweakUV8.ip";
connectAttr "polyTweak14.out" "polyMergeVert10.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert10.mp";
connectAttr "polyTweakUV8.out" "polyTweak14.ip";
connectAttr "polyMergeVert10.out" "polyTweakUV9.ip";
connectAttr "polyTweak15.out" "polyMergeVert11.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert11.mp";
connectAttr "polyTweakUV9.out" "polyTweak15.ip";
connectAttr "polyMergeVert11.out" "polyTweakUV10.ip";
connectAttr "polyTweak16.out" "polyMergeVert12.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert12.mp";
connectAttr "polyTweakUV10.out" "polyTweak16.ip";
connectAttr "polyMergeVert12.out" "polyTweakUV11.ip";
connectAttr "polyTweak17.out" "polyMergeVert13.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert13.mp";
connectAttr "polyTweakUV11.out" "polyTweak17.ip";
connectAttr "polyMergeVert13.out" "polyTweakUV12.ip";
connectAttr "polyTweak18.out" "polyMergeVert14.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert14.mp";
connectAttr "polyTweakUV12.out" "polyTweak18.ip";
connectAttr "polyMergeVert14.out" "polyTweakUV13.ip";
connectAttr "polyTweak19.out" "polyMergeVert15.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert15.mp";
connectAttr "polyTweakUV13.out" "polyTweak19.ip";
connectAttr "polyMergeVert15.out" "polyTweakUV14.ip";
connectAttr "polyTweak20.out" "polyMergeVert16.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert16.mp";
connectAttr "polyTweakUV14.out" "polyTweak20.ip";
connectAttr "polyMergeVert16.out" "polyTweakUV15.ip";
connectAttr "polyTweak21.out" "polyMergeVert17.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert17.mp";
connectAttr "polyTweakUV15.out" "polyTweak21.ip";
connectAttr "polyMergeVert17.out" "polyTweakUV16.ip";
connectAttr "polyTweak22.out" "polyMergeVert18.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert18.mp";
connectAttr "polyTweakUV16.out" "polyTweak22.ip";
connectAttr "polyMergeVert18.out" "polyTweakUV17.ip";
connectAttr "polyTweak23.out" "polyMergeVert19.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert19.mp";
connectAttr "polyTweakUV17.out" "polyTweak23.ip";
connectAttr "polyMergeVert19.out" "polyTweakUV18.ip";
connectAttr "polyTweak24.out" "polyMergeVert20.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert20.mp";
connectAttr "polyTweakUV18.out" "polyTweak24.ip";
connectAttr "polyMergeVert20.out" "polyTweakUV19.ip";
connectAttr "polyTweak25.out" "polyMergeVert21.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert21.mp";
connectAttr "polyTweakUV19.out" "polyTweak25.ip";
connectAttr "polyMergeVert21.out" "polyTweakUV20.ip";
connectAttr "polyTweak26.out" "polyMergeVert22.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert22.mp";
connectAttr "polyTweakUV20.out" "polyTweak26.ip";
connectAttr "polyMergeVert22.out" "polyTweakUV21.ip";
connectAttr "polyTweak27.out" "polyMergeVert23.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert23.mp";
connectAttr "polyTweakUV21.out" "polyTweak27.ip";
connectAttr "polyMergeVert23.out" "polyTweakUV22.ip";
connectAttr "polyTweak28.out" "polyMergeVert24.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert24.mp";
connectAttr "polyTweakUV22.out" "polyTweak28.ip";
connectAttr "polyMergeVert24.out" "polyTweakUV23.ip";
connectAttr "polyTweak29.out" "polyMergeVert25.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert25.mp";
connectAttr "polyTweakUV23.out" "polyTweak29.ip";
connectAttr "polyMergeVert25.out" "polyTweakUV24.ip";
connectAttr "polyTweak30.out" "polyMergeVert26.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert26.mp";
connectAttr "polyTweakUV24.out" "polyTweak30.ip";
connectAttr "polyMergeVert26.out" "polyBevel1.ip";
connectAttr "pCubeShape1.wm" "polyBevel1.mp";
connectAttr "polyBevel1.out" "polyTweakUV25.ip";
connectAttr "polyTweak31.out" "polyMergeVert27.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert27.mp";
connectAttr "polyTweakUV25.out" "polyTweak31.ip";
connectAttr "polyMergeVert27.out" "polyTweakUV26.ip";
connectAttr "polyTweak32.out" "polyMergeVert28.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert28.mp";
connectAttr "polyTweakUV26.out" "polyTweak32.ip";
connectAttr "polyMergeVert28.out" "polyTweakUV27.ip";
connectAttr "polyTweak33.out" "polyMergeVert29.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert29.mp";
connectAttr "polyTweakUV27.out" "polyTweak33.ip";
connectAttr "polyMergeVert29.out" "polyTweakUV28.ip";
connectAttr "polyTweak34.out" "polyMergeVert30.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert30.mp";
connectAttr "polyTweakUV28.out" "polyTweak34.ip";
connectAttr "polyMergeVert30.out" "polyBevel2.ip";
connectAttr "pCubeShape1.wm" "polyBevel2.mp";
connectAttr "polyBevel2.out" "polyTweakUV29.ip";
connectAttr "polyTweak35.out" "polyMergeVert31.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert31.mp";
connectAttr "polyTweakUV29.out" "polyTweak35.ip";
connectAttr "polyMergeVert31.out" "polyTweakUV30.ip";
connectAttr "polyTweak36.out" "polyMergeVert32.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert32.mp";
connectAttr "polyTweakUV30.out" "polyTweak36.ip";
connectAttr "polyMergeVert32.out" "polyTweak37.ip";
connectAttr "polyTweak37.out" "polySplit1.ip";
connectAttr "polySplit1.out" "polySplit2.ip";
connectAttr "polyTweak38.out" "polyBevel3.ip";
connectAttr "pCubeShape1.wm" "polyBevel3.mp";
connectAttr "polySplit2.out" "polyTweak38.ip";
connectAttr "polyBevel3.out" "polyTweakUV31.ip";
connectAttr "polyTweak39.out" "polyMergeVert33.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert33.mp";
connectAttr "polyTweakUV31.out" "polyTweak39.ip";
connectAttr "polyMergeVert33.out" "polyTweakUV32.ip";
connectAttr "polyTweak40.out" "polyMergeVert34.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert34.mp";
connectAttr "polyTweakUV32.out" "polyTweak40.ip";
connectAttr "polyMergeVert34.out" "polyTweakUV33.ip";
connectAttr "polyTweak41.out" "polyMergeVert35.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert35.mp";
connectAttr "polyTweakUV33.out" "polyTweak41.ip";
connectAttr "polyMergeVert35.out" "polyTweakUV34.ip";
connectAttr "polyTweak42.out" "polyMergeVert36.ip";
connectAttr "pCubeShape1.wm" "polyMergeVert36.mp";
connectAttr "polyTweakUV34.out" "polyTweak42.ip";
connectAttr "polyTweak43.out" "polySoftEdge3.ip";
connectAttr "pCubeShape1.wm" "polySoftEdge3.mp";
connectAttr "polyMergeVert36.out" "polyTweak43.ip";
connectAttr "polySoftEdge3.out" "polySmoothFace1.ip";
connectAttr "polySmoothFace1.out" "deleteComponent1.ig";
connectAttr "deleteComponent1.og" "deleteComponent2.ig";
connectAttr "deleteComponent2.og" "deleteComponent3.ig";
connectAttr "deleteComponent3.og" "deleteComponent4.ig";
connectAttr "deleteComponent4.og" "deleteComponent5.ig";
connectAttr "deleteComponent5.og" "deleteComponent6.ig";
connectAttr "deleteComponent6.og" "deleteComponent7.ig";
connectAttr "deleteComponent7.og" "deleteComponent8.ig";
connectAttr "deleteComponent8.og" "deleteComponent9.ig";
connectAttr "deleteComponent9.og" "deleteComponent10.ig";
connectAttr "deleteComponent10.og" "deleteComponent11.ig";
connectAttr "deleteComponent11.og" "deleteComponent12.ig";
connectAttr "deleteComponent12.og" "deleteComponent13.ig";
connectAttr "deleteComponent13.og" "deleteComponent14.ig";
connectAttr "deleteComponent14.og" "deleteComponent15.ig";
connectAttr "deleteComponent15.og" "deleteComponent16.ig";
connectAttr "deleteComponent16.og" "deleteComponent17.ig";
connectAttr "deleteComponent17.og" "deleteComponent18.ig";
connectAttr "deleteComponent18.og" "deleteComponent19.ig";
connectAttr "deleteComponent19.og" "deleteComponent20.ig";
connectAttr "deleteComponent20.og" "deleteComponent21.ig";
connectAttr "deleteComponent21.og" "deleteComponent22.ig";
connectAttr "deleteComponent22.og" "deleteComponent23.ig";
connectAttr "deleteComponent23.og" "deleteComponent24.ig";
connectAttr "deleteComponent24.og" "deleteComponent25.ig";
connectAttr "deleteComponent25.og" "deleteComponent26.ig";
connectAttr "deleteComponent26.og" "deleteComponent27.ig";
connectAttr "deleteComponent27.og" "deleteComponent28.ig";
connectAttr "deleteComponent28.og" "deleteComponent29.ig";
connectAttr "deleteComponent29.og" "deleteComponent30.ig";
connectAttr "deleteComponent30.og" "deleteComponent31.ig";
connectAttr "deleteComponent31.og" "deleteComponent32.ig";
connectAttr "deleteComponent32.og" "polyTweak44.ip";
connectAttr "polyTweak44.out" "deleteComponent33.ig";
connectAttr "deleteComponent33.og" "deleteComponent34.ig";
connectAttr "deleteComponent34.og" "deleteComponent35.ig";
connectAttr "deleteComponent35.og" "deleteComponent36.ig";
connectAttr "deleteComponent36.og" "deleteComponent37.ig";
connectAttr "deleteComponent37.og" "deleteComponent38.ig";
connectAttr "deleteComponent38.og" "deleteComponent39.ig";
connectAttr "deleteComponent39.og" "deleteComponent40.ig";
connectAttr "deleteComponent40.og" "deleteComponent41.ig";
connectAttr "polySphere1.out" "polySoftEdge4.ip";
connectAttr "pSphereShape1.wm" "polySoftEdge4.mp";
connectAttr "polySurfaceShape1.o" "polySoftEdge5.ip";
connectAttr "pSphereShape2.wm" "polySoftEdge5.mp";
connectAttr "deleteComponent41.og" "polyExtrudeFace8.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace8.mp";
connectAttr "polyTweak45.out" "polySplitRing10.ip";
connectAttr "pCubeShape1.wm" "polySplitRing10.mp";
connectAttr "polyExtrudeFace8.out" "polyTweak45.ip";
connectAttr "polyTweak46.out" "polyBevel4.ip";
connectAttr "pCubeShape1.wm" "polyBevel4.mp";
connectAttr "polySplitRing10.out" "polyTweak46.ip";
connectAttr "polyTweak47.out" "polySoftEdge6.ip";
connectAttr "pCubeShape1.wm" "polySoftEdge6.mp";
connectAttr "polyBevel4.out" "polyTweak47.ip";
connectAttr "polySoftEdge6.out" "deleteComponent42.ig";
connectAttr "deleteComponent42.og" "deleteComponent43.ig";
connectAttr "deleteComponent43.og" "polyDelEdge1.ip";
connectAttr "polyDelEdge1.out" "polyDelEdge2.ip";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "pCubeShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pSphereShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pSphereShape2.iog" ":initialShadingGroup.dsm" -na;
// End of enemy.ma
