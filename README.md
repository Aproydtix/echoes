# Echoes
Made in Unity using C#.

In Echoes, you walk around in a maze-like world and have to use a sound/light wave to reveal your surroundings. You have to traverse this maze to find an exit point to win the game. As you walk around and use your echo tool, you will make enemies move towards you. These enemies you can hear, but only see if you hit them with your echo tool. If any of these enemies hit you, you die, and lose the game.

For this I did the player logic, enemy logic, input methods, compass, and overall game systems.

# Controls
Created for XInput Gamepads.

- Move:                     Left Stick
- Camera:                   Right Stick
- L or R Shoulderbuttons: 	Echo

Mouse and keyboard is also supported, but is there for testing purposes and as such is not optimal.

## Credits
### Art
- Vetle Rossland
### Programming
- Eirik Hiis-Hauge
- Martin L
### Level Design
- Hal94
### Game Design
- Bjørn K. Aune

## Build
A build can be found as a .zip file in the Downloads section.

## Video Demo
https://youtu.be/ppIzZFdHP0A

## Screenshots
![Screenshot1](Screenshot1.png)
![Screenshot2](Screenshot2.png)
![Screenshot3](Screenshot3.png)